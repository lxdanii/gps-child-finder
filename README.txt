# README #


### 1 GPS CHILD FINDER ###
	Where is my child? This is an android application that shows the location of your child on a map. 
	You can connect a GPS-Bluetooth-device with your mobile phone which sends the current GPS-position
	of your child over Bluetooth to the phone. The app shows the location (e.g. on a large playground) and 
	also the orientation of the device/of your child on a map.

	
### 2 INSTALLATION ###

2.1 Develop and run GPS Child Finder

	!! The instructions are applicable for Windows 7 (64 bit) systems.

	2.1.1 To run the application in a common building tool the following software should be installed
	on your device:

		1. JDK (Java SE Development Kit latest version):
			<http://www.oracle.com/technetwork/java/javase/downloads/index.html>

		2. Eclipse IDE for Java Developers (3.7.2 or greater):
			<http://eclipse.org/downloads/>

		3. Eclipse JDT plugin (if NOT already included in the Eclipse IDE package):
			<http://www.eclipse.org/jdt/>		

		4. Android SDK:
			<http://developer.android.com/sdk/index.html>

		5. Android ADT Plugin for Eclipse:
			Eclipse --> Help --> Install New Software --> Add... Name: "ADT Plugin", Location: "https://dl-ssl.google.com/
			android/eclipse/" --> Accept the license agreements and install the Developer Tools --> Restart Eclipse --> 
			Add the Android SDK directory (you've downloaded and unpacked before) to the Android Preferences

		6. Git:
			<http://git-scm.com/download/win>

	2.1.2 If all the software is set up you have to pull the GPS Child Finder project repository:

		1. Open Git and initialize a git repository on a chosen directory (git init).
		2. Pull https://bitbucket.org/lxdanii/gps-child-finder.git

	2.1.3 To run GPS Child Finder in Eclipse the following steps should be made:

		1. Open Eclipse and select a workspace (not the GPS Child Finder directory!)

		2. Import --> Android --> Existing Android Code Into Workspace --> Root Directory Browse... --> Select the directory
			where you pulled the GPS Child Finder project and select both projects ("google-play-services_lib" and 
			"GPSChildFinder") --> finish

		3. Select the GPSChildFinder project --> Run --> Run as... --> Android Application
		
		Remember that the gen folder (Generated Java Files) is created when the project is build. If the gen folder isn't
		generated automatically, it's necessary to building the project manually (Project --> Build Project).

		If the "Android AVD Error" appears and asks if you want to add a new Android Virtual Device:
		
			 1. Press YES
			 
			 2. Plug in your Android phone via USB and let it install the driver software. USB-Debugging should be
				enabled on your phone!
			 
			 3. Select "Choose a running Android device" and select your android device.
			 
			 4. GPS Child Finder will be started on you phone.
		 
		!! If the android device isn't shown jet, check if the drivers are correct. If not, install the Google USB driver
			manually from <http://developer.android.com/sdk/win-usb.html>.
		 
		!! An emulator is not possible because the application needs GPS, Bluetooth and other things that an emulator can't
			simulate.

2.2 Run on android phone

	!! GPS Child Finder can only be used on Android Devices with version 4.0 (Ice Cream Sandwich) or higher.

	For running the application directly on your android phone, you have to install it on your phone. 
	For that its necessary to copy the GPSChildFinder.apk file from the projects directory to a known
	direction on your phones storage (e.g. into the downloads folder) and install it from there manually by
	clicking on it. With the help of a common file data manager (e.g. File Manager from the Google Play Store)
	you can switch to the chosen direction.

	
### 3 3RD PARTY SOFTWARE & LIBRARIES ###

3.1 Google Play Services Lib

	To run the application in a common building tool, its necessary to make the Google Play Services API
	available for GPSChildFinder in your workspace. When the git repository is cloned, the 
	google-play-services_lib project should be available and GPSChildFinder should have referenced the 
	Google Play services library project.
		
	To run the application directly on your android phone, the phone should have Google Play Services 
	installed. If that's not the case, GPSChildFinder will tell you and shows you a way to install it.
	
3.2 Android Support v4 (android-support-v4.jar)

	Available in the GPSChildFinder/libs folder and referenced in the Build Path.
	Assists with development of applications for android API level 4 or later.
	<http://developer.android.com/tools/support-library/index.html>

3.3 SLF4J Android (slf4j-android-1.6.1-RC1.jar)

	Available in the GPSChildFinder/libs folder and referenced in the BuildPath.
	Logging Framework for the application. 
	<http://www.slf4j.org/>

	
### 4 GPS-BLUETOOTH-DEVICE ###

	The application was developed and tested with the **BT-GPS-8 Jentro** device. For more information 
	read <http://www.handytechnik.de/assets/files/bedienungsanleitungen/Jentro%20activepilot%20GPS%20Maus
	/89150_Jentro-bt-gps-8_UM.pdf>.

	GPS Child Finder should also work with any other Bluetooth Device which sends NMEA sentences to the 
	phone it's connected with. But I can't guarantee for that because the application isn't a matured 
	software at this point.

	
### 5 AVAILABILITY ###

5.1 Source code: on Bitbucket <https://bitbucket.org/lxdanii/gps-child-finder/src>

5.2 Documentation: in the GPS Child Finder project folder doc

	
### 6 COPYRIGHT (c) 2014 ###

	Author: Daniela Blum
	E-mail: <e1025570@student.tuwien.ac.at>

	GPS Child Finder was developed under GNU GENERAL PUBLIC LICENCE V3 (see full licence in the project directory:
	LICENCE.txt).