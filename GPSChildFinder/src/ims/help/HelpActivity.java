package ims.help;

import ims.gpschildfinder.R;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

/**
 * This class' components are declared in the belonging layout xml file
 * (activity_help.xml). It gives a short overview to the user on the main
 * components and how to use the application. It's composed of pictures of the
 * main components with short description. <br>
 * <br>
 * See activity_help.xml<br>
 * <br>
 * 
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class HelpActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.i(getClass().getName(), "ON CREATE");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);
	}
}
