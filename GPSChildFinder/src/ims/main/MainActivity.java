package ims.main;

import ims.devicemanager.BluetoothConnectThread;
import ims.devicemanager.DeviceHandler;
import ims.gpschildfinder.R;
import ims.help.HelpActivity;
import ims.map.Map;
import ims.settings.SettingsActivity;
import ims.settings.SettingsFragment;

import com.google.android.gms.maps.GoogleMap;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

/**
 * This activity is the main activity of the application. It instantiates a
 * {@link ims.map.Map Map.class} for the main use of the application. It creates
 * the action bar with the icon menu, handles the Icons and shows necessary
 * dialogs and toasts. <br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */

public class MainActivity extends Activity {

	private GpsChildFinder gpsChildFinder;
	private Map map;
	private SettingsFragment settingsFragment;
	private Menu menu;
	private Toast toast;

	/**
	 * First it calls the {@link Activity#onCreate(Bundle) super.onCreate}
	 * method and sets the content view and the activity context. It passes the
	 * instance of this activity to the application class (
	 * {@link ims.main.GpsChildFinder}) and sets the current activity variable
	 * to this instance. It instantiates a Map.class and calls the
	 * {@link ims.map.Map#initMap() initMap} method from the instantiated
	 * {@link ims.map.Map} class to initialize a Google Map and its components.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.i(getClass().getName(), "ON CREATE");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		gpsChildFinder = (GpsChildFinder) this.getApplicationContext();
		gpsChildFinder.setCurrentActivity(this);
		gpsChildFinder.setMainActivity(this);

		DeviceHandler.disconnectedToastShown(true);

		map = new Map(this, this.getFragmentManager());
		map.initMap();
	}

	/**
	 * First it calls the {@link Activity#onResume() super.onResume} method and
	 * sets the current activity in the application class (
	 * {@link ims.main.GpsChildFinder}) variable to this instance. <br>
	 * <br>
	 * The {@link #handleBluetoothIcon(Menu)} method is called for handling the
	 * Bluetooth icon on the action bar. <br>
	 * Additionally it calls the {@link #showDeviceList()} method to show the
	 * paired devices in a dialog when no Bluetooth-Device is connected (via
	 * {@link ims.devicemanager.BluetoothConnectThread#DEVICE_CONNECTED
	 * BluetoothConnectThread.DEVICE_CONNECTED}) variable. The location access
	 * is checked (if the user turned it off).
	 */
	@Override
	protected void onResume() {
		Log.i(getClass().getName(), "ON RESUME");
		gpsChildFinder.setCurrentActivity(this);
		super.onResume();
		if (menu != null) {
			handleBluetoothIcon(menu);
		}

		if (!BluetoothConnectThread.DEVICE_CONNECTED) {
			showDeviceList();
		}

		if (map != null) {
			map.checkLocationSettings();
		}
	}

	/**
	 * Called in {@link onResume()} method after the inquiry returns that no
	 * Bluetooth device is connected. Schedules a fragment transaction, opens a
	 * hidden {@link ims.settings.SettingsFragment SettingsFragment} and hence
	 * the {@link ims.settings.DeviceListPreference DeviceListPreference} that
	 * the user can choose between paired devices to connect.
	 * 
	 */
	private void showDeviceList() {
		FragmentTransaction fragmentTransaction = getFragmentManager()
				.beginTransaction();
		settingsFragment = new SettingsFragment();
		fragmentTransaction.replace(android.R.id.content, settingsFragment);
		fragmentTransaction.hide(settingsFragment);
		fragmentTransaction.commit();
	}

	/**
	 * Called, when the {@link ims.settings.DeviceListPreference
	 * DeviceListPreference} dialog is closed by the user or dismissed by
	 * another event. It removes the {@link ims.settings.SettingsFragment
	 * SettingsFragment} from the screen.
	 * 
	 */
	public void removeSettingsFragment() {
		FragmentTransaction fragmentTransaction = getFragmentManager()
				.beginTransaction();
		fragmentTransaction.remove(settingsFragment);
		fragmentTransaction.commit();
	}

	/**
	 * Shows a toast on the screen with a message about the status that the
	 * current Bluetooth device is disconnected. This method is called from the
	 * {@link ims.map.Map} when the current Bluetooth-device is disconnected.
	 */
	public void showDisconnectedToast() {
		toast = Toast.makeText(this, R.string.disconnected_toast,
				Toast.LENGTH_LONG);
		toast.show();
	}

	/**
	 * Called when the toast with the message about the status, that the current
	 * Bluetooth device is disconnected, should be cancelled. This method is
	 * necessary, because this toast should not be shown everytime (especially
	 * at the first initialization of
	 * {@link ims.devicemanager.BluetoothConnectThread BluetoothConnectThread})
	 */
	public void cancelDisconnectedToast() {
		if (toast != null) {
			toast.cancel();
		}
	}

	/**
	 * Additionally to the functions of the overridden method, it calls the
	 * {@link #handleBluetoothIcon(Menu)} method to handle the Bluetooth-icon on
	 * the action bar.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		this.menu = menu;
		getMenuInflater().inflate(R.menu.menu_main, this.menu);
		handleBluetoothIcon(this.menu);
		return true;
	}

	/**
	 * Called within the {@link #onCreateOptionsMenu(Menu)} and the
	 * {@link #onResume()} method. Handles the Bluetooth-icon on the action bar.
	 * Is a Bluetooth device connected the icon turns blue, otherwise it sets
	 * the grey icon.
	 * 
	 * @param menu
	 *            The managing icon menu
	 */
	private void handleBluetoothIcon(Menu menu) {
		MenuItem bluetoothIcon = menu.findItem(R.id.icon_bluetooth);
		if (BluetoothConnectThread.DEVICE_CONNECTED) {
			bluetoothIcon.setIcon(R.drawable.ic_action_bluetooth_connected);
		} else {
			bluetoothIcon.setIcon(R.drawable.ic_action_bluetooth);
		}
		
	}

	/**
	 * Called when a item gets selected by the user in the managing icon menu on
	 * the action bar.
	 * <ul>
	 * case <b>icon_bluetooth</b>: If no BT device is connected, a toast is shown<br>
	 * case <b>action_settings</b>: "Settings" is selected.
	 * {@link #showSettingsMenu()} is called. <br>
	 * case <b>action_help</b>: "Help" is selected. {@link #showHelp()} is
	 * called. <br>
	 * case <b>mapModeMenu</b>: "map mode" is selected.
	 * {@link #mapModeMenu(MenuItem)} is called. <br>
	 * </ul>
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.icon_bluetooth:
			if(!BluetoothConnectThread.DEVICE_CONNECTED) {
				toast = Toast.makeText(this, R.string.device_button_disconnected,
						Toast.LENGTH_SHORT);
				toast.show();
			}
			return true;
		case R.id.action_settings:
			showSettingsMenu();
			return true;
		case R.id.action_help:
			showHelp();
			return true;
		}
		if (mapModeMenu(item))
			return true;

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Called when "Help" is selected in the icon menu on the action bar. A
	 * {@link ims.help.HelpActivity HelpActivity} intent is created and started.
	 */
	private void showHelp() {
		Intent intent = new Intent(this, HelpActivity.class);
		startActivity(intent);
	}

	/**
	 * Called when "Settings" is selected in the icon menu on the action bar. A
	 * {@link ims.settings.SettingsActivity SettingsActivity} intent is created
	 * and started.
	 */
	private void showSettingsMenu() {
		Intent intent = new Intent(this, SettingsActivity.class);
		startActivity(intent);
	}

	/**
	 * Called when "map mode" is selected in the icon menu on the action bar.
	 * This method handles the status of the checkboxes from different
	 * map-mode-items. Is a item selected the others are set unchecked and the
	 * {@link ims.map.Map#setMapType(int) Map.setMapType(int)} is called to set
	 * the selected GoogleMap.MAP_TYPE.
	 * 
	 * @param item
	 *            The menu item that was selected.
	 * @return true if a map mode is selected, false otherwise
	 */
	private boolean mapModeMenu(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.mapmode_normal:
			if (item.isChecked())
				item.setChecked(false);
			else {
				item.setChecked(true);
				map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			}
			return true;
		case R.id.mapmode_satellite:
			if (item.isChecked())
				item.setChecked(false);
			else {
				item.setChecked(true);
				map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
			}
			return true;
		case R.id.mapmode_terrain:
			if (item.isChecked())
				item.setChecked(false);
			else {
				item.setChecked(true);
				map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
			}
			return true;
		}
		return false;
	}

	/**
	 * Exits the application.
	 */
	protected void exitApp() {
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	/**
	 * First it destroys calls the {@link ims.map.Map#onDestroy() Map.onDestroy}
	 * method when a map exists. Then {@link Activity#onDestroy()
	 * super.onDestroy()} is called.
	 */
	@Override
	protected void onDestroy() {
		Log.i(getClass().getName(), "ON DESTROY");
		if (map != null)
			map.onDestroy();

		super.onDestroy();
	}

	/**
	 * Called, when the back-button is pressed and exits the application.
	 */
	@Override
	public void onBackPressed() {
		exitApp();
	}

	/**
	 * After calling super.onStart(), reconnecting the location client after
	 * shutting it down to save battery power.
	 */
	@Override
	protected void onStart() {
		Log.i(getClass().getName(), "ON START");
		super.onStart();
		if (map != null)
			map.connectMyLocationClient();
	}

	/**
	 * Disconnecting the location client when the activity is no longer visible
	 * to save battery power and call super.onStop().
	 */
	@Override
	protected void onStop() {
		Log.i(getClass().getName(), "ON STOP");
		if (map != null)
			map.disconnectMyLocationClient();
		super.onStop();
	}

}
