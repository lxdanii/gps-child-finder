package ims.main;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import ims.gpschildfinder.R;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

/**
 * This class is called at the start of the application. It's responsible for
 * checking whether the required (Bluetooth service, Internet access, location
 * access) and recommended (GPS) services are activated and turned on or not
 * before the MainActivity is started. Also, it checks if the Google Play
 * Services are available on the phone. <br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class StartingActivity extends Activity {

	private GpsChildFinder gpsChildFinder;

	private AlertDialog dialogRequiredServices;

	private Dialog dialogGooglePlayError;
	
	private Toast toast;

	/**
	 * This handler handles the start of the following activity - the
	 * MainActivity - after the required checks were executed and returned true.
	 */
	private Handler startActivityHandler;

	private BluetoothAdapter BLUETOOTH_ADAPTER;

	private NetworkInfo netInfo;

	final int RQS_GooglePlayServices = 1;

	/**
	 * Called when the activity is starting. First it calls the
	 * {@link Activity#onCreate(Bundle) super.onCreate} method and sets the
	 * content view and the activity context. It passes the instance of this
	 * activity to the application class ({@link ims.main.GpsChildFinder}) and
	 * sets the current activity variable to this instance. Also the
	 * startActivityHandler is initialized witch handles the start of the
	 * MainAcitivity.class.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_starting);
		gpsChildFinder = (GpsChildFinder) this.getApplicationContext();
		gpsChildFinder.setCurrentActivity(this);
		gpsChildFinder.setStartingActivity(this);

		/**
		 * This handler handles the start of the following activity - the
		 * MainActivity.
		 */
		startActivityHandler = new Handler() {

			/**
			 * Is called when all the required services (and optional the
			 * recommended services) are activated.
			 */
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case 1:
					Intent intent = new Intent(
							gpsChildFinder.getStartingActivity(),
							MainActivity.class);
					startActivity(intent);

				default:
					break;
				}
			}
		};
	}

	/**
	 * First, {@link Activity#onResume() super.onResume} method is called, it
	 * calls the checkForRequiredServices()-method witch checks whether the
	 * required and recommended services are activated and turned on and the
	 * google play services are available on or not. If the method both return
	 * <b>true</b>, the startActivityHandler is called to run the following
	 * activity.
	 */
	@Override
	protected void onResume() {
		Log.i(getClass().getName(), "ON RESUME");
		gpsChildFinder.setCurrentActivity(this);
		super.onResume();

		if (dialogRequiredServices != null)
			dialogRequiredServices.cancel();

		if (dialogGooglePlayError != null)
			dialogGooglePlayError.cancel();
		
		if (checkForGooglePlayServices()) {
			if (checkForRequiredServices()) {
				startActivityHandler.sendEmptyMessageDelayed(1, 1000);
			}
		}

	}

	/**
	 * Before the MainActivity with the main functions and the map is called, it
	 * makes sure that google play services are available on the phone. This
	 * services are needed to run the application.
	 * 
	 * @return <b>true</b> if google play services is available, otherwise
	 *         <b>false</b>
	 */
	private boolean checkForGooglePlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getApplicationContext());

		if (resultCode == ConnectionResult.SUCCESS) {
			Log.d("REQUIRED SERVICES", "google play services: " + "true");
			return true;
		} else {
			dialogGooglePlayError = GooglePlayServicesUtil.getErrorDialog(resultCode, this,
					RQS_GooglePlayServices);
			dialogGooglePlayError.setCanceledOnTouchOutside(false);
			dialogGooglePlayError.show();
			
			Log.d("REQUIRED SERVICES", "google play services: " + "false");
			return false;
		}

	}

	/**
	 * Before the MainActivity with the main functions and the map is called, it
	 * makes sure that the following required and recommended services are
	 * activated and calls the responsible methods:
	 * <ul>
	 * 1. bluetooth services ({@link #checkBluetoothServices()
	 * checkBluetoothServices}) <br>
	 * 2. internet connection ({@link #checkInternetConnection()
	 * checkInternetConnection})<br>
	 * 3. location access ({@link #checkLocationAccess() checkLocationAccess})<br>
	 * 4. GPS (recommended) ({@link #checkGpsServices() checkGpsServices})<br>
	 * </ul>
	 * Is at least one service not turned on, the
	 * {@link #showDialogRequiredServices(boolean, boolean, boolean, boolean)
	 * showDialogRequiredServices} Method is called witch shows a dialog.
	 * 
	 * @return <b>true</b> if all the required and recommended services are
	 *         turned on, otherwise <b>false</b>
	 */

	public boolean checkForRequiredServices() {

		boolean bluetoothServices = checkBluetoothServices();
		Log.d("REQUIRED SERVICES", "Bluetooth services: " + bluetoothServices);

		boolean internetConnection = checkInternetConnection();
		Log.d("REQUIRED SERVICES", "internet connection: " + internetConnection);

		boolean locationAccess = checkLocationAccess();
		Log.d("REQUIRED SERVICES", "location access: " + locationAccess);

		boolean gpsServices = checkForGpsServices();
		Log.d("RECOMMENDED SERVICES", "gps services: " + gpsServices);

		if (!bluetoothServices || !internetConnection || !locationAccess
				|| !gpsServices) {
			Log.i("REQUIRED SERVICES",
					"show dialog of required/recommended services");
			showDialogRequiredServices(bluetoothServices, internetConnection,
					locationAccess, gpsServices);
		}

		return (bluetoothServices && internetConnection && locationAccess && gpsServices);

	}

	/**
	 * Checks whether the gps service is turned on or not. This service is only
	 * recommended (for better precision) and are not necessary for the
	 * application.
	 * 
	 * @return true if the gps services are turned on, otherwise false
	 */
	private boolean checkForGpsServices() {
		if (((LocationManager) getSystemService(LOCATION_SERVICE))
				.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			return true;
		}
		return false;
	}

	/**
	 * Checks whether the location access is granted or rather turned on or not.
	 * This service is required for running the application to find the
	 * locations. <br>
	 * <br>
	 * For older versions than android KITKAT it uses a depreciated method.
	 * 
	 * @return true if the location access is granted or rather turned on,
	 *         otherwise false
	 */
	@TargetApi(Build.VERSION_CODES.KITKAT)
	private boolean checkLocationAccess() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			int locationMode = Settings.Secure.LOCATION_MODE_OFF;
			try {
				locationMode = Settings.Secure.getInt(getContentResolver(),
						Settings.Secure.LOCATION_MODE);

			} catch (SettingNotFoundException e) {
				e.printStackTrace();
			}

			return (locationMode != Settings.Secure.LOCATION_MODE_OFF && (locationMode == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY || locationMode == Settings.Secure.LOCATION_MODE_BATTERY_SAVING)); // check

		} else {
			@SuppressWarnings("deprecation")
			String locationInfo = Settings.Secure.getString(
					getContentResolver(),
					Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
			if (locationInfo.contains("gps")
					|| locationInfo.contains("network")) {
				return true;
			}
			return false;
		}
	}

	/**
	 * Checks whether the Internet connectivity service is turned on or not.
	 * This service is required for running the application to show the map and
	 * the locations.
	 * 
	 * @return true if the Internet connection service is turned on, otherwise
	 *         false
	 */
	private boolean checkInternetConnection() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	/**
	 * Checks whether the Bluetooth service is turned on or not. This service is
	 * required for running the application to show connect to an
	 * GPS-Bluetooth-Device. <br>
	 * <br>
	 * The application is exited if the mobile phone doesn't support Bluetooth.
	 * 
	 * @return true if the Bluetooth services are turned on, otherwise false
	 */
	private boolean checkBluetoothServices() {
		BLUETOOTH_ADAPTER = BluetoothAdapter.getDefaultAdapter();

		if (BLUETOOTH_ADAPTER == null) {
			showToast(R.string.no_bluetooth_support, Toast.LENGTH_SHORT);
			exitApp();
		}
		GpsChildFinder.BLUETOOTH_ADAPTER = BLUETOOTH_ADAPTER;

		if (BLUETOOTH_ADAPTER.isEnabled()
				|| BLUETOOTH_ADAPTER.getState() == BluetoothAdapter.STATE_TURNING_ON
				|| BLUETOOTH_ADAPTER.getState() == BluetoothAdapter.STATE_ON) {
			return true;
		}

		return false;
	}

	/**
	 * Shows dialog when at least one required or recommended service is turned
	 * off. <br>
	 * <br>
	 * The dialog outlines a list of the disabled services and a button
	 * "Settings" which is redirects to the phones settings to turn them on. <br>
	 * If required services are turned off the
	 * {@link #setupDialogRequiredServices(AlertBuilder)
	 * setupDialogRequiredServices} method is called. <br>
	 * If only recommended services are turned off and all required are
	 * activated the {@link #setupDialogRecommendedServices(AltertBuilder)
	 * setupDialogRecommendedServices} method is called.
	 * 
	 * @param bluetoothServices
	 *            Bluetooth service enabled/disabled
	 * @param internetConnection
	 *            Internet connection (WiFi, mobile data) is is turned on/off
	 * @param locationAccess
	 *            location access is turned on/off
	 * @param gpsServices
	 *            GPS is turned on/off
	 */
	private void showDialogRequiredServices(final boolean bluetoothServices,
			final boolean internetConnection, final boolean locationAccess,
			final boolean gpsServices) {

		boolean onlyRecommendedServices = false;
		if (bluetoothServices && internetConnection && locationAccess
				&& !gpsServices)
			onlyRecommendedServices = true;

		StringBuilder sb = new StringBuilder();

		sb.append(getString(R.string.request_required_services));
		if (!bluetoothServices) {
			sb.append("\n" + getString(R.string.bluetooth_services));
		}
		if (!internetConnection) {
			sb.append("\n" + getString(R.string.internet_connection));
		}
		if (!locationAccess) {
			sb.append("\n" + getString(R.string.location_access));
		}
		if (!gpsServices) {
			sb.append("\n" + getString(R.string.gps_services));
		}
		sb.append("\n" + getString(R.string.request_required_services_1));

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder
				.setTitle(R.string.request_required_services_title)
				.setMessage(sb.toString())
				.setPositiveButton(R.string.title_activity_settings,
						new DialogInterface.OnClickListener() {

							/**
							 * Redirects to the settings menu of the phone to
							 * turn the services on.
							 */
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								Intent enableServicesIntent = new Intent(
										android.provider.Settings.ACTION_SETTINGS);
								gpsChildFinder.getStartingActivity()
										.startActivity(enableServicesIntent);
							}
						});

		if (onlyRecommendedServices) {
			alertDialogBuilder = setupDialogRecommendedServices(alertDialogBuilder);
		} else {
			alertDialogBuilder = setupDialogRequiredServices(alertDialogBuilder);
		}

		dialogRequiredServices = alertDialogBuilder.create();
		dialogRequiredServices.setCanceledOnTouchOutside(false);
		dialogRequiredServices.show();

	}

	/**
	 * Called when required services are turned off and extends the
	 * requiredServicesDialog with a "Cancel"-button witch exits the
	 * application, when the user refuses to turn the required services on.
	 * There is no use of the application then because it malfunction without
	 * one of these services. <br>
	 * 
	 * @param alertDialogBuilder
	 *            The alertBialogBuilder to extend.
	 * @return
	 */
	private Builder setupDialogRequiredServices(Builder alertDialogBuilder) {
		alertDialogBuilder.setNegativeButton(R.string.cancel,
				new DialogInterface.OnClickListener() {
					/**
					 * The user refuses to turn the required services on so the
					 * application is exited.
					 */
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
						showToast(R.string.required_dialog_canceled,
								Toast.LENGTH_LONG);
						exitApp();
					}
				});
		alertDialogBuilder.setOnKeyListener(new OnKeyListener() {

			/**
			 * The user refuses to turn the required services on so the
			 * application is exited.
			 */
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					dialog.dismiss();
					showToast(R.string.required_dialog_canceled,
							Toast.LENGTH_LONG);
					exitApp();
				}
				return false;
			}

		});
		return alertDialogBuilder;
	}

	/**
	 * Called when only recommended services are turned off and extends the
	 * requiredServicesDialog with a a "Ignore"-button. So the user is able to
	 * skip activating the services and go on with the application, because the
	 * recommended services aren't necessary for running the application.
	 * 
	 * @param alertDialogBuilder
	 * @return
	 */
	private Builder setupDialogRecommendedServices(Builder alertDialogBuilder) {
		alertDialogBuilder.setNegativeButton(R.string.ignore,
				new DialogInterface.OnClickListener() {
					/**
					 * Starts the MainActivity whether the services are turned
					 * on or not.
					 */
					public void onClick(DialogInterface dialog, int id) {
						startActivityHandler.sendEmptyMessageDelayed(1, 1000);
					}
				});
		alertDialogBuilder.setOnKeyListener(new OnKeyListener() {
			/**
			 * Starts the MainActivity whether the services are turned on or
			 * not.
			 */
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					startActivityHandler.sendEmptyMessageDelayed(1, 1000);
				}
				return false;
			}

		});
		return alertDialogBuilder;
	}

	/**
	 * Shows a toast on the screen.
	 * 
	 * @param message
	 *            Current relevant message to show on the screen. The resource
	 *            id of the string resource to use. Can be formatted text.
	 * @param duration
	 *            How long to display the message. Either
	 *            {@link android.widget.Toast#LENGTH_SHORT Toast.LENGTH_SHORT}
	 *            or {@link android.widget.Toast.LENGTH_LONG Toast#LENGTH_LONG}
	 */
	protected void showToast(int message, int duration) {
		toast = Toast.makeText(gpsChildFinder.getStartingActivity(), message,
				duration);
		toast.show();

	}

	/**
	 * Exits the application.
	 */
	protected void exitApp() {
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	/**
	 * First calles the {@link Activity#onPause() super.onPause} method and
	 * dismisses the dialog if its still shown on the screen.
	 */
	@Override
	public void onPause() {
		super.onPause();
		if (dialogRequiredServices != null)
			dialogRequiredServices.dismiss();
	}

	/**
	 * Called, when the back-button is pressed and exits the application.
	 */
	@Override
	public void onBackPressed() {
		exitApp();
	}
}
