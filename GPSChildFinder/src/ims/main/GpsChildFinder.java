package ims.main;

import ims.settings.SettingsActivity;
import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;

/**
 * Base class to maintain global application state. This class stores the
 * instances of all the used activities and provides them to the whole
 * application, packages and classes. <br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class GpsChildFinder extends Application {

	/** The current activity in the foreground of the application */
	private Activity currentActivity;

	/** Is set when the startingActivity is called the first time in onCreate() */
	private StartingActivity startingActivity;

	/** Is set when the mainActivity is called the first time in onCreate() */
	private MainActivity mainActivity;

	/** Is set when the settingsActivity is called the first time in onCreate() */
	private SettingsActivity settingsActivity;

	/**
	 * The Bluetooth adapter witch is set in the startingActivity after
	 * Bluetooth service is turned on
	 */
	public static BluetoothAdapter BLUETOOTH_ADAPTER;

	/**
	 * @return the current activity in the foreground
	 */
	public Activity getCurrentActivity() {
		return currentActivity;
	}

	/**
	 * @param currentActivity
	 *            to set
	 */
	public void setCurrentActivity(Activity currentActivity) {
		this.currentActivity = currentActivity;
	}

	/**
	 * Updates the screen depending on the current activity. <br>
	 * In case of mainActivity the Options menu is invalidated
	 * {@link ims.main.MainActivity#invalidateOptionsMenu()
	 * MainActivity.invalidateOptionsMenu()}, otherwise (in case of the
	 * settingsActivity) the {@link ims.settings.SettingsActivity#restart()
	 * SettingsActivity.restart()} is called.
	 */
	public void updateScreen() {
		try {
			if (currentActivity.getClass().equals(MainActivity.class)) {
				mainActivity.runOnUiThread(new Runnable() {
				     @Override
				     public void run() {
				    	 mainActivity.invalidateOptionsMenu();
				    }
				});

				
			} else if (currentActivity.getClass()
					.equals(SettingsActivity.class)) {
				settingsActivity.restart();
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the instantiated MainActivity
	 */
	public MainActivity getMainActivity() {
		return mainActivity;
	}

	/**
	 * 
	 * @param mainActivity
	 *            to set
	 */
	public void setMainActivity(MainActivity mainActivity) {
		this.mainActivity = mainActivity;
	}

	/**
	 * 
	 * @return the instantiated SettingActivity
	 */
	public SettingsActivity getSettingsActivity() {
		return settingsActivity;
	}

	/**
	 * 
	 * @param settingsActivity
	 *            to set
	 */
	public void setSettingsActivity(SettingsActivity settingsActivity) {
		this.settingsActivity = settingsActivity;
	}

	/**
	 * 
	 * @return the instantiated startingActivity
	 */
	public StartingActivity getStartingActivity() {
		return startingActivity;
	}

	/**
	 * 
	 * @param startingActivity
	 *            to set
	 */
	public void setStartingActivity(StartingActivity startingActivity) {
		this.startingActivity = startingActivity;
	}

}
