package ims.settings;

import ims.devicemanager.BluetoothConnectThread;
import ims.devicemanager.DeviceHandler;
import ims.gpschildfinder.R;
import ims.main.GpsChildFinder;
import ims.main.MainActivity;

import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.preference.ListPreference;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.Log;

/**
 * Represents the device list preference from the settings fragment, which shows
 * the paired devices that the user can choose one to connect with the phone.<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class DeviceListPreference extends ListPreference {

	/** instance of this class */
	protected DeviceListPreference deviceListPreference;

	/** current activity from which this preference is invoked */
	private Activity currentActivity;

	/** number of paired devices */
	private Integer numberOfDevices = null;

	/** list of paired devices */
	private Set<BluetoothDevice> pairedDevices;

	/**
	 * Constructor. <br>
	 * Initializes this preference.
	 * 
	 * @param context
	 *            preference's context
	 */
	public DeviceListPreference(Context context) {
		super(context, null);
		deviceListPreference = this;
		init();
	}

	/**
	 * Constructor. <br>
	 * Initializes this preference.
	 * 
	 * @param context
	 *            preference's context
	 * @param attrs
	 *            list of attributes
	 */
	public DeviceListPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		deviceListPreference = this;
		init();
	}

	/**
	 * Initializes this preference. The summary is set and the first
	 * initialization of the list of the paired devices is made. <br>
	 * As well, the {@link OnPreferenceClickListener} is set, which handles
	 * click events on the preference and more important, the
	 * {@link OnPreferenceChangeListener} is set, which calls a new
	 * {@link ims.devicemanager.BluetoothConnectThread} when another device is
	 * selected.
	 */
	public void init() {
		this.setPersistent(false);
		setSummary();
		initDeviceList();
		setOnPreferenceClickListener(new OnPreferenceClickListener() {

			/**
			 * If the preference is clicked, the device list is shown in a
			 * dialog.
			 */
			@Override
			public boolean onPreferenceClick(Preference preference) {
				initDeviceList();
				return true;
			}

		});

		setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

			/**
			 * If a new Bluetooth-device is selected by the user, the old
			 * {@link ims.devicemanager.BluetoothConnectThread} is closed, the
			 * summary is updated and a new
			 * {@link ims.devicemanager.BluetoothConnectThread} is opened with
			 * the selected device.
			 */
			@Override
			public boolean onPreferenceChange(Preference preference,
					Object newValue) {
				if (!pairedDevices.isEmpty() && newValue != null) {
					String btAdress = (String) newValue;
					for (BluetoothDevice btDevice : pairedDevices) {
						if (btDevice.getAddress().equals(btAdress)
								&& !btDevice.equals(DeviceHandler
										.getConnectedDevice())) {
							setSummary(btDevice.getName());
							try { // destroy old BluetoothConnectThread
								DeviceHandler.getBtConnectThread().cancel(true);
							} catch (NullPointerException e) {
							}
							;
							BluetoothConnectThread.getInstance(getContext(),
									btDevice, currentActivity);
							break;
						}
					}
					return true;
				}
				return false;
			}

		});
	}

	/**
	 * Setup of the summary text. If a device is successfully connected, the
	 * name is shown.
	 */
	public void setSummary() {
		if (!BluetoothConnectThread.DEVICE_CONNECTED) {
			setSummary(R.string.pref_choose_device);
		} else {
			setSummary(DeviceHandler.getConnectedDevice().getName());
			this.setValue(DeviceHandler.getConnectedDevice().getAddress());
		}
	}

	/**
	 * Prepares the dialog builder to be shown when the preference is clicked.
	 * The list of with the phone paired devices is shown and two buttons are
	 * initialized. The neutral button redirects to the settings menu of the
	 * phone to add a new Bluetooth device to the list.
	 */
	@Override
	public void onPrepareDialogBuilder(Builder builder) {
		super.onPrepareDialogBuilder(builder);
		builder.setNeutralButton(R.string.pref_new_device,
				new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						currentActivity
								.startActivityForResult(
										new Intent(
												android.provider.Settings.ACTION_BLUETOOTH_SETTINGS),
										0);
					}
				});
		builder.setNegativeButton(R.string.cancel, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (currentActivity.getClass().equals(MainActivity.class)) {
					((MainActivity) currentActivity).removeSettingsFragment();
				}
				if (getDialog() != null)
					getDialog().dismiss();
			}

		});
	}

	/**
	 * resets the dialog.
	 */
	private void resetDialog() {
		if (getDialog() != null)
			getDialog().dismiss();
		onClick();

	}

	/**
	 * 
	 * @param currentActivity
	 *            to set
	 */
	public void setActivity(Activity currentActivity) {
		this.currentActivity = currentActivity;
	}

	/**
	 * 
	 * @return currentActivity to get
	 */
	public Activity getActivity() {
		return currentActivity;
	}

	/**
	 * Initializes the list of the Bluetooth Devices, which are paired with the
	 * mobile phone.
	 */
	public void initDeviceList() {
		Log.i("INIT DEVICE LIST", "INIT DEVICE LIST");
		
		if(GpsChildFinder.BLUETOOTH_ADAPTER.getBondedDevices() != null) {
			pairedDevices = GpsChildFinder.BLUETOOTH_ADAPTER.getBondedDevices();
		}
		

		CharSequence[] entries = new CharSequence[pairedDevices.size()];
		CharSequence[] entryValues = new CharSequence[pairedDevices.size()];

		if (pairedDevices.isEmpty()) {
			this.setDialogMessage(R.string.pref_gps_bt_device_empty);
		} else {
			int i = 0;
			for (BluetoothDevice btDevice : pairedDevices) {
				entries[i] = btDevice.getName();
				entryValues[i] = btDevice.getAddress();
				i++;
			}
		}
		setEntries(entries);
		setEntryValues(entryValues);

		/** If the device list has changed */
		if (getDialog() != null && numberOfDevicesChanged(pairedDevices.size())) {
			resetDialog();
		}
	}

	/**
	 * Called to ask if the number of the paired devices in the device list has
	 * changed.
	 * 
	 * @param newNumberOfDevices
	 *            the new number of devices to compare with
	 * @return true if the number has changed, false otherwise
	 */
	private boolean numberOfDevicesChanged(int newNumberOfDevices) {
		if (numberOfDevices == null) {
			numberOfDevices = newNumberOfDevices;
			return false;
		}
		if (numberOfDevices != newNumberOfDevices) {
			numberOfDevices = newNumberOfDevices;
			return true;
		}
		return false;
	}

	/**
	 * Wrapper method to show the dialog.
	 */
	public void show() {
		showDialog(null);
	}

	/**
	 * Wrapper method to dismiss the dialog and remove the SettingsFragment if
	 * necessary.
	 */
	public void dismiss() {
		if (currentActivity.getClass().equals(MainActivity.class))
			((MainActivity) currentActivity).removeSettingsFragment();
		if (getDialog() != null)
			getDialog().dismiss();

	}

	protected void onDialogClosed(boolean positiveResult) {
		super.onDialogClosed(positiveResult);
		try {
			setSummary();
		} catch (NullPointerException e) {
		}
	}
}
