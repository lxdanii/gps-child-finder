package ims.settings;


import ims.gpschildfinder.R;
import ims.main.MainActivity;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceFragment;

/**
 * Extends the {@link PreferenceFragment} and shows a hierarchy of the settings
 * preferences.<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class SettingsFragment extends PreferenceFragment {

	/** list of devices to connect */
	private DeviceListPreference deviceList;

	/**
	 * checked by the user if the direction should be shown, unchecked otherwise
	 */
	private CheckBoxPreference directionCheckbox;

	/**
	 * Dialog which asks if the user want's to disconnect the current device
	 * conneced
	 */
	private DisconnectPreference disconnectPreference;

	/**
	 * Adds the preferences from the preferences resource and calls the
	 * {@link #initDeviceListPreference()} and {@link #initDirectionCheckbox()}
	 * to setup the preferences.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferences);
		initDeviceListPreference();
		initDisconnectPreference();
		initDirectionCheckbox();
	}

	/**
	 * Initializes the {@link #deviceList}. <br>
	 * Finds the preference and shows the preference if the method is called
	 * from the {@link MainActivity} class.
	 */
	private void initDeviceListPreference() {
		deviceList = (DeviceListPreference) findPreference("pref_gps_bt_device");
		deviceList.setActivity(this.getActivity());
		if (deviceList.getActivity().getClass().equals(MainActivity.class)) {
			deviceList.show();
			deviceList.getDialog().setCanceledOnTouchOutside(false);
		}
	}

	/**
	 * Initializes the {@link #disconnectPreference}. <br>
	 */
	private void initDisconnectPreference() {
		disconnectPreference = (DisconnectPreference) findPreference("pref_disconnect");
	}

	/**
	 * Initializes the {@link #directionCheckbox}. <br>
	 * Finds the preference and sets up the {@link OnPreferenceChangeListener}
	 * for listening to preference changes (if the direction should be shown or
	 * not - chosen by the user).
	 */
	private void initDirectionCheckbox() {
		directionCheckbox = (CheckBoxPreference) findPreference("pref_direction");

		directionCheckbox
				.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

					/**
					 * If the newValue-object is true, the directionCheckbox
					 * gets checked and
					 * {@link SettingsActivity#DIRECTION_CHECKED} is set true. <br>
					 * Otherwise, if the newValue-object is false, the
					 * directionCheckbox gets unchecked and
					 * {@link SettingsActivity#DIRECTION_CHECKED} is set false.
					 */
					@Override
					public boolean onPreferenceChange(Preference preference,
							Object newValue) {
						boolean checked = (Boolean) newValue;
						if (checked) {
							directionCheckbox.setChecked(true);
							SettingsActivity.DIRECTION_CHECKED = true;

						} else {
							directionCheckbox.setChecked(false);
							SettingsActivity.DIRECTION_CHECKED = false;
						}
						return false;
					}

				});

	}

	@Override
	public void onPause() {
		super.onPause();
		if (deviceList != null) {
			deviceList.dismiss();
		}
		if (disconnectPreference != null) {
			disconnectPreference.dismiss();
		}
	}
}
