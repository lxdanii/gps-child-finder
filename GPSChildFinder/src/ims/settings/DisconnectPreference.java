package ims.settings;

import ims.devicemanager.BluetoothConnectThread;
import ims.devicemanager.DeviceHandler;
import ims.gpschildfinder.R;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.preference.DialogPreference;
import android.util.AttributeSet;

/**
 * Represents the disconnect preference from the settings fragment.<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class DisconnectPreference extends DialogPreference {

	/**
	 * Constructor.<br>
	 * Enables the dialog for this preference.
	 * 
	 * @param context
	 *            preference's context
	 */
	public DisconnectPreference(Context context) {
		super(context, null);
		enableDialog();
	}

	/**
	 * Constructor.<br>
	 * Enables the dialog for this preference.
	 * 
	 * @param context
	 *            preference's context
	 */
	public DisconnectPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		enableDialog();
	}

	/**
	 * Prepares the dialog builder to be shown when the preference is clicked.
	 * It shows the message, if the current GPS-Bluetooth-device should be
	 * disconnected and choices for the user to react.
	 */
	public void onPrepareDialogBuilder(Builder builder) {
		builder.setMessage(R.string.pref_disconnect_msg);
		builder.setPositiveButton(R.string.yes, new OnClickListener() {

			/**
			 * The current device gets disconnected or rather the
			 * Bluetooth-connection-thread gets canceled.
			 */
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (DeviceHandler.getBtConnectThread() != null) {
					DeviceHandler.getBtConnectThread().cancel(true);
				}
			}

		});
		builder.setNegativeButton(R.string.no, null);
	}

	/**
	 * Enables or rather disables the dialog. <br>
	 * If a device is connected, the dialog gets disabled, else enabled.
	 */
	private void enableDialog() {
		if (!BluetoothConnectThread.DEVICE_CONNECTED) {
			setEnabled(false);
		} else {
			setEnabled(true);
		}
	}

	/**
	 * Wrapper method for dismissing the dialog.
	 */
	public void dismiss() {
		if (getDialog() != null)
			getDialog().dismiss();
	}
}
