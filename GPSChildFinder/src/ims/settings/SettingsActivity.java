package ims.settings;

import ims.gpschildfinder.R;
import ims.help.HelpActivity;
import ims.main.GpsChildFinder;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Represents the Settings of the application.<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */

public class SettingsActivity extends Activity {

	private static GpsChildFinder gpsChildFinder;

	/** true if the direction should be shown, false otherwise */
	public static Boolean DIRECTION_CHECKED;
	private Menu menu;

	/**
	 * Displays the {@link SettingsFragment} as the main content.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		gpsChildFinder = (GpsChildFinder) this.getApplicationContext();
		gpsChildFinder.setCurrentActivity(this);
		gpsChildFinder.setSettingsActivity(this);

		getFragmentManager().beginTransaction()
				.replace(android.R.id.content, new SettingsFragment()).commit();
	}

	/**
	 * Restarts the settings activity.
	 */
	public void restart() {
		Log.d(getClass().getName(), "RESTART...");
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}

	public void onResume() {
		Log.i(getClass().getName(), "ON RESUME");
		super.onResume();
		gpsChildFinder.setCurrentActivity(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		this.menu = menu;
		getMenuInflater().inflate(R.menu.menu_settings, this.menu);
		return true;
	}

	/**
	 * When a options item is selected, this method is called. <br>
	 * MenuItem can only be ation_help which starts the
	 * {@link ims.help.HelpActivity}.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_help:
			Log.d(getLocalClassName(), item.getTitle() + " pressed");
			Intent intent = new Intent(this, HelpActivity.class);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
