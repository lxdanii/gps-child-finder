package ims.devicemanager;

import ims.gpschildfinder.R;
import ims.main.GpsChildFinder;
import ims.main.MainActivity;

import java.io.IOException;
import java.util.UUID;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.widget.Toast;

/**
 * If a Bluetooth-Device is selected from the device list this class is called.
 * It opens sockets to connect with the device and manages the connection (calls
 * the {@link BluetoothDataManager} class if the connection stands) until
 * shutdown. <br>
 * <br>
 * 
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class BluetoothConnectThread extends Thread {

	/** instance of this class */
	private static BluetoothConnectThread instance = null;

	private GpsChildFinder gpsChildFinder;

	private UUID DEFAULT_UUID = UUID
			.fromString("00001101-0000-1000-8000-00805F9B34FB");

	/** current Bluetooth socket to connect with the Bluetooth-device */
	private final BluetoothSocket bluetoothSocket;

	/** current Bluetooth-device */
	private final BluetoothDevice bluetoothDevice;

	/**
	 * BluetoothDataManager which gets called when a connection with the device
	 * stands
	 */
	private BluetoothDataManager bluetoothDataManager;

	/**
	 * dialog which gets shown when the Bluetooth socket tries to connect with a
	 * device
	 */
	private ProgressDialog connectDialog;

	/** true if the connect dialog got canceled, false otherwise */
	private boolean connectDialogCanceled = false;

	private Context context;

	/** current activity from which this preference is invoked */
	private Activity currentActivity;

	private Toast toast;

	/**
	 * true if a device is connected via Bluetooth or rather a Bluetooth socket
	 * is connected to a remote device
	 */
	public static boolean DEVICE_CONNECTED = false;

	/**
	 * true if the phone receives GPS input from the GPS-Bluetooth-device, false
	 * otherwise
	 */
	public static boolean GETTING_GPS_INPUT = false;

	/**
	 * Sets up the Bluetooth connect thread, values of the {@link DeviceHandler}
	 * and the {@link BluetoothDataManager} if the connection stands. Creates a
	 * Bluetooth-socket ready to start a connection and starts the thread for
	 * trying to connect to the BT-device during the connection dialog is shown.
	 * 
	 * @param context
	 *            current context
	 * @param device
	 *            device to connect
	 * @param currentActivity
	 *            current activity
	 */
	private BluetoothConnectThread(Context context, BluetoothDevice device,
			Activity currentActivity) {
		this.context = context;
		this.currentActivity = currentActivity;
		gpsChildFinder = (GpsChildFinder) context.getApplicationContext();
		gpsChildFinder.getMainActivity().cancelDisconnectedToast();

		BluetoothSocket tmp = null;
		bluetoothDevice = device;
		DeviceHandler.setConnectedDevice(device);
		DeviceHandler.setBtConnectThread(this);

		try {
			tmp = bluetoothDevice
					.createRfcommSocketToServiceRecord(DEFAULT_UUID);
		} catch (IOException e) {
		}
		bluetoothSocket = tmp;
		bluetoothDataManager = new BluetoothDataManager(bluetoothSocket);
		DeviceHandler.setBtDataManager(bluetoothDataManager);
		showConnectionDialog();

		Thread t = new Thread(this);
		t.start();
	}

	public static BluetoothConnectThread getInstance(Context context,
			BluetoothDevice device, Activity currentActivity) {
		if (instance == null) {
			instance = new BluetoothConnectThread(context, device,
					currentActivity);
		}
		return instance;
	}

	/**
	 * Tries to connect the Bluetooth socket with the Bluetooth device when no
	 * device is connected so far. If the connect dialog gets cancelled the
	 * process gets canceled. <br>
	 * <br>
	 * If the device is connected,
	 * {@link BluetoothDataManager#readNMEASentences()} is called to handle the
	 * incomming sentences from the device.
	 */
	public void run() {

		GpsChildFinder.BLUETOOTH_ADAPTER.cancelDiscovery();

		while (!DEVICE_CONNECTED && !connectDialogCanceled) {
			try {

				Log.d(getClass().getName(), "connect...");
				currentActivity
						.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
				bluetoothSocket.connect();

				Log.d(getClass().getName(), "connected");
				DEVICE_CONNECTED = true;
				connectDialog.dismiss();
				currentActivity
						.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
				gpsChildFinder.updateScreen();

			} catch (IOException connectException) {

				connectDialog.dismiss();
				currentActivity
						.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
				if (!connectDialogCanceled) {
					showTimeoutToast();
				}
				connectDialogCanceled = true;

			}
		}

		if (DEVICE_CONNECTED && !connectDialogCanceled) {
			Log.d(getClass().getName(), "connected!");
			try {
				while (true) {
					bluetoothDataManager.readNMEASentences();
				}
			} catch (IOException connectException) {
				cancel(false);
				return;
			}
		} else {
			cancel(false);
			return;
		}

	}

	/**
	 * Does the connection trial takes to long, the process is cancelled, the
	 * socket closed and this toast is shown on the main UI thread.
	 */
	private void showTimeoutToast() {
		gpsChildFinder.getMainActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				toast = Toast.makeText(context,
						R.string.connect_dialog_timeout, Toast.LENGTH_LONG);
				toast.show();
			}
		});

	}

	/**
	 * Displays the connection dialog with a process circle during the Bluetooth
	 * socket tries to connect with the Bluetooth device.
	 */
	private void showConnectionDialog() {

		connectDialogCanceled = false;

		String title = context.getString(R.string.connect_dialog_title) + " "
				+ bluetoothDevice.getName();

		connectDialog = new ProgressDialog(context);
		connectDialog.setTitle(title);
		connectDialog.setMessage(context
				.getString(R.string.connect_dialog_message));
		connectDialog.setButton(ProgressDialog.BUTTON_NEGATIVE,
				context.getString(R.string.cancel),
				new DialogInterface.OnClickListener() {

					/**
					 * The dialog gets cancelled and the process of trying to
					 * connect with a device gets cancelled and the socket
					 * closed.
					 */
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Log.i(getClass().getName(), "Connect-Dialog canceled");
						connectDialogCanceled = true;
						try {
							bluetoothSocket.close();
						} catch (IOException e) {
						}

						if (currentActivity.getClass().equals(
								MainActivity.class)) {
							((MainActivity) currentActivity)
									.removeSettingsFragment();
						}
					}

				});
		connectDialog.setCancelable(true);
		connectDialog.setOnCancelListener(new OnCancelListener() {

			/**
			 * The dialog gets cancelled and the process of trying to connect
			 * with a device gets cancelled and the socket closed.
			 */
			@Override
			public void onCancel(DialogInterface dialog) {
				Log.i(getClass().getName(), "Connect-Dialog canceled");
				dialog.dismiss();
				connectDialogCanceled = true;
				try {
					bluetoothSocket.close();
				} catch (IOException e) {
				}

				if (currentActivity.getClass().equals(MainActivity.class)) {
					((MainActivity) currentActivity).removeSettingsFragment();
				}
			}

		});

		connectDialog.show();
	}

	/**
	 * Called when something goes wrong with the Bluetooth-connection or the
	 * user want's to disconnect the device from the phone. <br>
	 * Cancels the connection of the socket and the Bluetooth device, calls the
	 * {@link DeviceHandler#cleanUp()} method, cleans out the values and updates
	 * the screen if necessary.
	 * 
	 * @param triggered
	 *            true if the user triggered the cancellation of the connection,
	 *            false otherwise
	 */
	public void cancel(boolean triggered) {
		Log.i(getClass().getName(), "Device disconnected");
		DEVICE_CONNECTED = false;
		GETTING_GPS_INPUT = false;
		connectDialogCanceled = false;
		DeviceHandler.cleanUp();
		instance = null;
		try {
			bluetoothSocket.close();

		} catch (IOException e) {
		}

		if (!triggered) {
			gpsChildFinder.updateScreen();
		}

	}
}
