package ims.devicemanager;

import android.bluetooth.BluetoothDevice;

/**
 * Handles the important values and information needed for the connected
 * GPS-Bluetooth-device. Provides all the important information about the device
 * and the Bluetooth connect thread. <br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class DeviceHandler {

	/** currently connected device */
	private static BluetoothDevice connectedDevice;

	/**
	 * bluetooth connect thread that is currently running and connected with the
	 * device
	 */
	private static BluetoothConnectThread btConnectThread;

	/**
	 * current bluetooth data manager to receive the data from the connected
	 * device
	 */
	private static BluetoothDataManager btDataManager;

	/**
	 * true if the toast - when the current device was disconnected - was shown
	 * before, false otherwise
	 */
	private static boolean disconnectedToastShown;

	/**
	 * 
	 * @return bluetooth connect thread that is currently running and connected
	 *         with the device
	 */
	public static BluetoothConnectThread getBtConnectThread() {
		return btConnectThread;
	}

	/**
	 * 
	 * @param btConnectThread
	 *            to set
	 */
	public static void setBtConnectThread(BluetoothConnectThread btConnectThread) {
		DeviceHandler.btConnectThread = btConnectThread;
	}

	/**
	 * 
	 * @return current bluetooth data manager to receive the data from the
	 *         connected device
	 */
	public static BluetoothDataManager getBtDataManager() {
		return btDataManager;
	}

	/**
	 * 
	 * @param btDataManager
	 *            to set
	 */
	public static void setBtDataManager(BluetoothDataManager btDataManager) {
		DeviceHandler.btDataManager = btDataManager;
	}

	/**
	 * 
	 * @return currently connected device
	 */
	public static BluetoothDevice getConnectedDevice() {
		return connectedDevice;
	}

	/**
	 * 
	 * @param connectedDevice
	 *            to set
	 */
	public static void setConnectedDevice(BluetoothDevice connectedDevice) {
		DeviceHandler.connectedDevice = connectedDevice;
	}

	/**
	 * Called when the DeviceHandler needs to be cleaned up after disconnect the
	 * current device.
	 */
	public static void cleanUp() {
		setConnectedDevice(null);
		setBtConnectThread(null);
		setBtDataManager(null);
	}

	/**
	 * 
	 * @return the status of the disconnectedToastShown (true if the toast -
	 *         when the current device was disconnected - was shown before,
	 *         false otherwise)
	 */
	public static boolean disconnectedToastShown() {
		return disconnectedToastShown;
	}

	/**
	 * 
	 * @param shown
	 *            boolean to set
	 */
	public static void disconnectedToastShown(boolean shown) {
		disconnectedToastShown = shown;
	}
}
