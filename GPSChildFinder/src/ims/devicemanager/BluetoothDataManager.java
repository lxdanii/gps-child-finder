package ims.devicemanager;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Arrays;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

/**
 * This class provides the main components that are needed for connecting with a
 * Bluetooth device, hold the connection and to receive and read the input of
 * the device.<br>
 * <br>
 * 
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class BluetoothDataManager {

	/** currently connected Bluetooth socket */
	private BluetoothSocket bluetoothSocket;

	private DataInputStream in;
	private BufferedReader br;

	/**
	 * Enumeration of hemisphere abbreviations. <br>
	 * Can either be {@link Hemisphere#N} for North, {@link Hemisphere#S} for
	 * South, {@link Hemisphere#W} for West or {@link Hemisphere#E} for East.
	 * 
	 * @author Daniela Blum
	 * 
	 */
	private enum Hemisphere {
		N, S, W, E
	}

	private Double[] currentCoordinates = new Double[2];
	private double latitude;
	private double longitude;

	/**
	 * Constructor which calls {@link setupStreamReader()} to read the input
	 * from the socket.
	 * 
	 * @param bluetoothSocket
	 *            currently connected Bluetooth socket
	 */
	public BluetoothDataManager(BluetoothSocket bluetoothSocket) {
		this.bluetoothSocket = bluetoothSocket;
		setupStreamReader();
	}

	/**
	 * Sets up the DataInputStream with the Bluetooth socket InputStream to read
	 * the incoming sentences. <br>
	 * If a IOException occurs, the Bluetooth socket is closed.
	 */
	private void setupStreamReader() {
		Log.i(getClass().getName(), "open InputStream");

		try {
			in = new DataInputStream(bluetoothSocket.getInputStream());

		} catch (IOException e) {
			try {
				bluetoothSocket.close();
				BluetoothConnectThread.GETTING_GPS_INPUT = false;
			} catch (IOException ex) {
				close();
			}
		}

	}

	/**
	 * Is called when the Bluetooth socket is connected to a Bluetooth device. <br>
	 * The method reads the sentences send from the device to the phone. When
	 * the sentences start with $GPGGA they are GPS NMEA scentences and contain
	 * the GPS coordinates of the device, so {@link #getCoordinates(String)} is
	 * called.
	 * 
	 * @throws IOException
	 *             when the connection get lost.
	 */
	public void readNMEASentences() throws IOException {
		StringBuffer sb = new StringBuffer();

		while (true) {
			char i = (char) in.read();
			if (i == '$' && sb.length() != 0) {
				String sentence = sb.toString();
				if (sentence.startsWith("$GPGGA"))
					getCoordinates(sentence);
				sb.setLength(0);
			}

			sb.append(i);
		}
	}

	/**
	 * Splits the input sentence from the device in peaces to extract the GPS
	 * coordinates with the help of the hemisphere abbreviations. If the
	 * sentence contains coordinates, the
	 * {@link BluetoothConnectThread#GETTING_GPS_INPUT} is set true, false
	 * otherwise. <br>
	 * <br>
	 * 
	 * @param sentence
	 */
	private void getCoordinates(String sentence) {

		String splittedSentence[] = sentence.split(",");
		String latitudeDegree;
		String latitudeHemisphere = splittedSentence[3];
		String longitudeDegree;
		String longitudeHemisphere = splittedSentence[5];
		if ((latitudeHemisphere.equals("N") || latitudeHemisphere.equals("S"))
				&& (longitudeHemisphere.equals("E") || longitudeHemisphere
						.equals("W"))) {
			latitudeDegree = splittedSentence[2];
			longitudeDegree = splittedSentence[4];

			try {
				Double coordinatesDecimal[] = {
						convertDegreeToDecimal(
								Hemisphere.valueOf(latitudeHemisphere),
								latitudeDegree),
						convertDegreeToDecimal(
								Hemisphere.valueOf(longitudeHemisphere),
								longitudeDegree) };

				setCoordinates(coordinatesDecimal);
				BluetoothConnectThread.GETTING_GPS_INPUT = true;
			} catch (NumberFormatException nfe) {
				Log.e(getClass().getName(),
						"No readable coordinates in NMEASentence.");
			}
		} else {
			Log.i(getClass().getName(), "Waiting for input... ");
			BluetoothConnectThread.GETTING_GPS_INPUT = false;
		}
	}

	/**
	 * Converts coordinates in degree format to decimal coordinates.
	 * 
	 * @param hp
	 *            hemisphere of the coordinate
	 * @param degreeCoordinate
	 *            coordinate in degree format
	 * @return the degreeCoordinate converteD to decimal coordinate as a double
	 * @throws NumberFormatException
	 *             occurs if the GPS coordinate can't be read or converted
	 */
	private Double convertDegreeToDecimal(Hemisphere hp, String degreeCoordinate)
			throws NumberFormatException {
		double degree = 0.0;
		double minuteSecond = 0.0;
		switch (hp) {
		case N: { // its the latitude
			degree = Double.valueOf(degreeCoordinate.substring(0, 2));
			minuteSecond = Double.valueOf(degreeCoordinate.substring(2,
					degreeCoordinate.length()));
		}
			break;
		case S: { // its the longitude
			degree = -1.0 * Double.valueOf(degreeCoordinate.substring(0, 2));
			minuteSecond = Double.valueOf(degreeCoordinate.substring(2,
					degreeCoordinate.length()));
		}
			break;
		case W: { // its the longitude
			degree = -1.0 * Double.valueOf(degreeCoordinate.substring(0, 3));
			minuteSecond = Double.valueOf(degreeCoordinate.substring(3,
					degreeCoordinate.length()));
		}
			break;
		case E: { // its the longitude
			degree = Double.valueOf(degreeCoordinate.substring(0, 3));
			minuteSecond = Double.valueOf(degreeCoordinate.substring(3,
					degreeCoordinate.length()));
		}
			break;
		}

		double decimalCoordinate = degree + (minuteSecond / 60.0);
		return decimalCoordinate;
	}

	/**
	 * Stores the coordinates into {@link #latitude} and {@link #longitude}.
	 * 
	 * @param coordinates
	 */
	private void setCoordinates(Double[] coordinates) {
		if (!Arrays.equals(currentCoordinates, coordinates)) {
			this.currentCoordinates = coordinates;
			this.latitude = coordinates[0];
			this.longitude = coordinates[1];
		}

	}

	/**
	 * 
	 * @return the current latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * 
	 * @return the current longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * Cleans up this class by closing the stream and reader and setting the
	 * {@link BluetoothConnectThread#GETTING_GPS_INPUT} to false.
	 */
	public void close() {
		try {
			in.close();
			br.close();
			BluetoothConnectThread.GETTING_GPS_INPUT = false;
		} catch (IOException e) {
		}

	}

}
