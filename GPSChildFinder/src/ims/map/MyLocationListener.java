package ims.map;

import android.hardware.GeomagneticField;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;

/**
 * Runnable which handles location changes of my location (phone's location).<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class MyLocationListener implements LocationListener {

	/**
	 * Map class in which the main components of the map and the
	 * {@link ims.map.Map#myLocationClient} is located, which handles this
	 * listener
	 */
	private Map map;

	// Milliseconds per second
	private static final int MILLISECONDS_PER_SECOND = 500;
	// Update frequency in seconds
	public static final int UPDATE_INTERVAL_IN_SECONDS = 1;
	// Update frequency in milliseconds
	private static final long UPDATE_INTERVAL = MILLISECONDS_PER_SECOND
			* UPDATE_INTERVAL_IN_SECONDS;
	// The fastest update frequency, in seconds
	private static final int FASTEST_INTERVAL_IN_SECONDS = 0;
	// A fast frequency ceiling in milliseconds
	private static final long FASTEST_INTERVAL = MILLISECONDS_PER_SECOND
			* FASTEST_INTERVAL_IN_SECONDS;

	private LocationRequest myLocationRequest;

	/**
	 * Constructor.
	 * 
	 * @param map
	 *            Map class in which the main components of the map and the
	 *            {@link ims.map.Map#myLocationClient} is located.
	 */
	public MyLocationListener(Map map) {
		this.map = map;

		setupMyLocationRequest();
	}

	private void setupMyLocationRequest() {
		myLocationRequest = LocationRequest.create();
		// Use high accuracy
		myLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		// Set the update interval to 5 seconds
		myLocationRequest.setInterval(UPDATE_INTERVAL);
		// Set the fastest update interval to 1 second
		myLocationRequest.setFastestInterval(FASTEST_INTERVAL);
	}

	/**
	 * Called when my location (phone's location) has changed. <br>
	 * First - is the location access still allowed by the user? Stores the new
	 * GPS coordinates to {@link ims.map.Map#myLocationLatLng} and asks for the
	 * declination for the compass mode.<br>
	 * At the end the location is shown on the map and set to the point of
	 * interest if {@link ims.map.Map#myLocationButton} is selected.
	 */
	@Override
	public void onLocationChanged(Location myLocation) {

		map.myLocationLatLng = new LatLng(myLocation.getLatitude(),
				myLocation.getLongitude());

		getDeclinationForCompassMode(myLocation);

		map.getMyLocation();

		if (map.getMyLocationButton().isSelected()) {
			Log.i("MyLocListener", "show location");
			map.showMyLocation();
		}
	}

	/**
	 * Called when the location is changed.<br>
	 * Estimates the magnetic field at a certain point, asks for the declination
	 * and updates {@link ims.map.Map#declination}. Declination is important for
	 * the {@link MySensorEventListener} to execute the compass mode.
	 * 
	 * @param myLocation
	 *            current location of the phone.
	 */
	private void getDeclinationForCompassMode(Location myLocation) {

		GeomagneticField field = new GeomagneticField(
				(float) myLocation.getLatitude(),
				(float) myLocation.getLongitude(),
				(float) myLocation.getAltitude(), System.currentTimeMillis());

		map.declination = field.getDeclination();

	}

	public LocationRequest getMyLocationRequest() {
		return myLocationRequest;
	}
}
