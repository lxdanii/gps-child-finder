package ims.map;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

/**
 * Implements the {@link #calculateNewBounds(GoogleMap, LatLng)} method which
 * calculates the bounds of the map's visible region.<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class BoundsCalculator {

	/**
	 * Calculates the new bounds to keep the center (of interest). If
	 * myLocationButton is pressed it's my location (phone's location); if the
	 * device's location button is pressed it's the device's location and show
	 * all markers in the visible region. <br>
	 * Its zoomed out starting from the minimum zoom level (21) until both
	 * locations are visible on the map.
	 * 
	 * @param map
	 *            GoogleMap to zoom out from and calculate the bounds
	 * @param otherLocation
	 *            which should also be visible on the map
	 */
	public static void calculateNewBounds(GoogleMap map, LatLng otherLocation) {

		double signLatitude = Math.signum(otherLocation.latitude);
		double signLongitude = Math.signum(otherLocation.longitude);

		double latitude = signLatitude
				* (Math.abs(otherLocation.latitude) + 0.0001);
		double longitude = signLongitude
				* (Math.abs(otherLocation.longitude) + 0.0001);
		LatLng extendedOtherLocation = new LatLng(latitude, longitude);

		int zoomLevel = 21;
		map.moveCamera(CameraUpdateFactory.zoomTo(zoomLevel));
		while (!(map.getProjection().getVisibleRegion().latLngBounds)
				.contains(extendedOtherLocation)) {
			zoomLevel -= 1;
			map.moveCamera(CameraUpdateFactory.zoomTo(zoomLevel));
		}
	}
}
