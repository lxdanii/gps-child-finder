package ims.map;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.MapFragment;

/**
 * MapFramgent for {@link ims.map.Map}. Extends
 * {@link com.google.android.gms.maps.MapFragment MapFragment} to use an
 * OnTouchListener.<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class OnTouchMapFragment extends MapFragment {

	/** Original View of the fragment */
	private View originalView;

	/** Wrapper layout of the fragment */
	private MapWrapperLayout mapWrapperLayout;

	/**
	 * Wrappes the original view in a {@link ims.map.MapWrapperLayout}.
	 * 
	 * @param inflater
	 *            Instantiates a layout XML file into its corresponding
	 *            {@link android.view.View} objects
	 * @param container
	 *            A ViewGroup is a special view that can contain other views
	 *            (called children)
	 * @param savedInstanceState
	 *            A mapping from String values to various Parcelable types
	 * @return mapWrapperLayout with the added original view
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		originalView = super.onCreateView(inflater, container,
				savedInstanceState);

		mapWrapperLayout = new MapWrapperLayout(getActivity());
		mapWrapperLayout.addView(originalView);

		return mapWrapperLayout;
	}

	@Override
	public View getView() {
		return originalView;
	}

	/**
	 * To set an {@link ims.map.MapWrapperLayout.OnTouchListener
	 * OnTouchListener} to the layout.
	 * 
	 * @param onTouchListener
	 *            the listener to set
	 */
	public void setOnTouchListener(
			MapWrapperLayout.OnTouchListener onTouchListener) {
		mapWrapperLayout.setOnTouchListener(onTouchListener);
	}
}