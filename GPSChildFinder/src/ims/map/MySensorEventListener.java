package ims.map;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Handles the rotation in direction of the view (phone's orientation) referring
 * to the compass mode after the specified amount of time elapses.<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class MySensorEventListener implements SensorEventListener {

	/**
	 * Map class in which the main components of the map.
	 */
	private Map map;

	/**
	 * Constructor.
	 * 
	 * @param map
	 *            Map class in which the main components of the map.
	 */
	public MySensorEventListener(Map map) {
		this.map = map;
	}

	/**
	 * Handles sensor events when the sensor changed. <br>
	 * It that case only the {@link Sensor#TYPE_ROTATION_VECTOR} type is
	 * interesting and gets handled and only when the
	 * {@link ims.map.Map#myLocationButton} is selected and the compass mode
	 * should be shown on the map. <br>
	 * The bearing (direction of the compass) is calculated with help of the
	 * orientation and the declination so that the map's camera can be updated (
	 * {@link updateCamera(float)}).
	 */
	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR
				&& map.getMyLocationButton().isSelected()) {
			float[] rotationMatrix = new float[16];
			SensorManager.getRotationMatrixFromVector(rotationMatrix,
					event.values);
			float[] orientation = new float[3];
			SensorManager.getOrientation(rotationMatrix, orientation);
			float bearing = (float) (Math.toDegrees(orientation[0]) + map.declination);
			updateCamera(bearing);
		}

	}

	/**
	 * Wrapper method calling {@link ims.map.Map#updateCamera(float)}
	 * 
	 * @param bearing
	 *            direction of the compass
	 */
	private void updateCamera(float bearing) {
		map.updateCamera(bearing);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

}
