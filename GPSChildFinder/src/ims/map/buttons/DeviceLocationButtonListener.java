package ims.map.buttons;

import ims.devicemanager.BluetoothConnectThread;
import ims.gpschildfinder.R;
import ims.map.Map;
import android.content.Context;
import android.view.View;
import android.widget.Toast;

/**
 * Listener associated with the {@link DeviceLocationButton} which listens to
 * click events.<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class DeviceLocationButtonListener implements View.OnClickListener {

	/**
	 * Map class in which the main components of the map.
	 */
	private Map map;

	/**
	 * Constructor.
	 * 
	 * @param map
	 *            Map class in which the main components of the map.
	 * @param context
	 *            Listener's and map's context
	 */
	public DeviceLocationButtonListener(Map map, Context context) {
		this.map = map;
	}

	/**
	 * Handles the click events. <br>
	 * If the button is clicked when no device is connected, a toast is shown
	 * with more information for the user. <br>
	 * Otherwise, the button is selected and the device location is shown or
	 * deselected.
	 */
	@Override
	public void onClick(View v) {
		if (!map.getDeviceLocationButton().isEnabled()) {
			if (!BluetoothConnectThread.DEVICE_CONNECTED) {
				map.showToast(R.string.device_button_disconnected,
						Toast.LENGTH_SHORT);
			} else if (!BluetoothConnectThread.GETTING_GPS_INPUT) {
				map.showToast(R.string.device_button_no_gps_input,
						Toast.LENGTH_SHORT);
			}
		} else {
			if (map.getDeviceLocationButton().isSelected()) {
				map.selectDeviceLocationButton(false);
			} else {
				map.selectDeviceLocationButton(true);

				map.showDeviceLocation(v);

				map.calculateNewBounds(this);
			}
		}
	}
}
