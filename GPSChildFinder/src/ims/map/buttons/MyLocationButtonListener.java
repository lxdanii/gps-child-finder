package ims.map.buttons;

import ims.gpschildfinder.R;
import ims.map.Map;
import android.content.Context;
import android.view.View;
import android.widget.Toast;

/**
 * Listener associated with the {@link MyLocationButton} which listens to click
 * events.<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class MyLocationButtonListener implements View.OnClickListener {

	/**
	 * Map class in which the main components of the map.
	 */
	private Map map;

	/**
	 * Constructor.
	 * 
	 * @param map
	 *            Map class in which the main components of the map.
	 * @param context
	 *            Listener's and map's context
	 */
	public MyLocationButtonListener(Map map, Context context) {
		this.map = map;
	}

	/**
	 * Handles the click events. <br>
	 * If the button is clicked when the location access is turned off or no
	 * connection to the location client can be received, a toast is shown with
	 * more information for the user. <br>
	 * Otherwise, the button is selected and my location (phone's location) is
	 * updated and shown or deselected.
	 */
	@Override
	public void onClick(View v) {
		if (!map.getMyLocationButton().isEnabled()) {
			if (!map.checkLocationSettings()) {
				map.showToast(R.string.location_access_OFF, Toast.LENGTH_SHORT);
			} else {
				map.showToast(R.string.location_client_OFF, Toast.LENGTH_SHORT);
			}
		} else {
			if (map.getMyLocationButton().isSelected()) {
				map.selectMyLocationButton(false);
			} else {
				map.selectMyLocationButton(true);

				map.requestMyLocationUpdate();

				map.showMyLocation();

				map.calculateNewBounds(this);
			}
		}
	}
}
