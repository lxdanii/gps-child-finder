package ims.map.buttons;

import ims.gpschildfinder.R;
import android.content.Context;
import android.util.AttributeSet;

/**
 * Button which is associated with the location of the device. It can be pressed
 * (for showing the device's location in the center of the map) or unpressed (to
 * stop the tracking of the location). Is the button disabled, the device's
 * location can't be received.<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class DeviceLocationButton extends StatusImageButton {

	public DeviceLocationButton(Context context) {
		super(context);
	}

	public DeviceLocationButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public DeviceLocationButton(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void setButtonStatus(ButtonStatus status) {
		switch (status) {
		case DISABLED: {
			setEnabled(false);
			buttonStatus = ButtonStatus.DISABLED;
			setBackgroundResource(R.drawable.location_button_disabled);
			break;
		}
		case PRESSED: {
			if (isEnabled()) {
				buttonStatus = ButtonStatus.PRESSED;
				setBackgroundResource(R.drawable.device_location_button_pressed);
			}
			break;
		}
		case UNPRESSED: {
			if (isEnabled()) {
				buttonStatus = ButtonStatus.UNPRESSED;
				setBackgroundResource(R.drawable.device_location_button);
			}
			break;
		}
		}

	}

}
