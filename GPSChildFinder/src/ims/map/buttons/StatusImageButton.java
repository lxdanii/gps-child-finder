package ims.map.buttons;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

/**
 * Abstract class which displays a button with an image. <br>
 * It can be pressed by the user and set enabled or disabled. This class extends
 * the {@link ImageButton} class with a method to set the button status and the
 * different associated images.<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public abstract class StatusImageButton extends ImageButton {

	/** true if button is enabled, false otherwise */
	boolean enabled;

	/**
	 * Enumeration of button's status. The button can be
	 * {@link ButtonStatus#UNPRESSED}, pressed {@link ButtonStatus#PRESSED} or
	 * disabled {@link ButtonStatus#DISABLED}.
	 * 
	 * @author Daniela Blum
	 * 
	 */
	public static enum ButtonStatus {
		UNPRESSED, PRESSED, DISABLED
	}

	/** the button's status */
	protected ButtonStatus buttonStatus;

	/**
	 * Constructor. <br>
	 * First calls the super(context), than sets the button enabled and the
	 * button status to {@link ButtonStatus#UNPRESSED}
	 * 
	 * @param context
	 *            button's context
	 */
	public StatusImageButton(Context context) {
		super(context);
		enabled = true;
		setButtonStatus(ButtonStatus.UNPRESSED);
	}

	/**
	 * Constructor. <br>
	 * First calls the super(context, AttributeSet), than sets the button
	 * enabled and the button status to {@link ButtonStatus#UNPRESSED}
	 * 
	 * @param context
	 *            button's context
	 * @param attrs
	 *            collection of attributes
	 */
	public StatusImageButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		enabled = true;
		setButtonStatus(ButtonStatus.UNPRESSED);
	}

	/**
	 * Constructor. <br>
	 * First calls the super(Context, AttributeSet, int), than sets the button
	 * enabled and the button status to {@link ButtonStatus#UNPRESSED}
	 * 
	 * @param context
	 *            button's context
	 * @param attrs
	 *            collection of attributes
	 * @param defStyle
	 *            default style to apply to this view
	 */
	public StatusImageButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		enabled = true;
		setButtonStatus(ButtonStatus.UNPRESSED);
	}

	/**
	 * 
	 * @return current button status
	 */
	public ButtonStatus getButtonStatus() {
		return buttonStatus;
	}

	/**
	 * The button status is set and the image is also changed.
	 * 
	 * @param status
	 *            button status to set. Can be {@link ButtonStatus#DISABLED},
	 *            {@link ButtonStatus#UNPRESSED} or {@link ButtonStatus#PRESSED}
	 * 
	 */
	public abstract void setButtonStatus(ButtonStatus status);

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public boolean isEnabled() {
		try {
			return enabled;
		} catch (NullPointerException e) {
			return super.isEnabled();
		}
	}

}
