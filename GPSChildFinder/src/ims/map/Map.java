package ims.map;

import ims.devicemanager.BluetoothConnectThread;
import ims.gpschildfinder.R;
import ims.main.GpsChildFinder;
import ims.map.MapWrapperLayout.OnTouchListener;
import ims.map.buttons.DeviceLocationButton;
import ims.map.buttons.DeviceLocationButtonListener;
import ims.map.buttons.MyLocationButton;
import ims.map.buttons.StatusImageButton.ButtonStatus;
import ims.map.buttons.MyLocationButtonListener;
import android.annotation.TargetApi;
import android.app.FragmentManager;
import android.content.Context;
import android.content.IntentSender;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * The main class which handles the map-fragment. It sets up the GoogleMap and
 * the main components.<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class Map implements GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {

	private GpsChildFinder gpsChildFinder;
	private Context context;

	private GoogleMap googleMap;

	private FragmentManager fragmentManager;

	/**
	 * <b>true</b> if the map is initialized and the {@link #startScreen} method
	 * should be called, <b>false</b> otherwise.
	 */
	private boolean initMap = false;

	private int locationMode = 0;

	/** Names of the active providers */
	private String locationProviders;

	/**
	 * Define a request code to send to Google Play services when
	 * {@link #myLocationClient} can't connect.
	 */
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	/**
	 * handles my location (the location of the phone) (location listener,
	 * register for location updates)
	 */
	private LocationClient myLocationClient;

	/**
	 * handles changes of my location and shows my location (phone's location)
	 * on the map (marker)
	 */
	private MyLocationListener myLocationListener;

	/** handles the location of the device (the child's location) */
	private Handler deviceLocationHandler;

	/**
	 * updates the location of the device after the specified amount of time
	 * elapses and shows the location of the device on the map (marker)
	 */
	private DeviceLocationListener updateDeviceLocationRunnable;

	/** stores the latitude and longitude of my location (phone's location) */
	LatLng myLocationLatLng;

	/** marker of my location shown on the map */
	Marker myLocationMarker;
	MarkerOptions myLocationMarkerOptions = new MarkerOptions();

	/** stores the latitude and longitude of the location of the device */
	LatLng deviceLocLatLng;

	/** marker of device's location shown on the map */
	Marker deviceLocationMarker;
	MarkerOptions deviceLocationMarkerOptions = new MarkerOptions();

	/**
	 * button of my location (phone's location) displayed in the upper right
	 * corner
	 */
	private MyLocationButton myLocationButton;

	/**
	 * button of the devices location (child's location) displayed in the upper
	 * right corner
	 */
	private DeviceLocationButton deviceLocationButton;

	/**
	 * Periodically updated distance between myLocationLatLng and
	 * deviceLocationLatLng is shown in this textView
	 */
	private TextView distance;

	/**
	 * Periodically updated distance between myLocationLatLng and
	 * deviceLocationLatLng
	 */
	float[] distanceInMeters = new float[1];

	/**
	 * <b>true</b> if the direction is turned on in settings menu and the
	 * devices location can be shown, <b>false</b> otherwise
	 */
	boolean showDirection = false;

	/**
	 * The declination of the horizontal component of the magnetic field from
	 * true north, in degrees <br>
	 * Gained with {@link android.hardware.GeomagneticField#getDeclination()
	 * GeomagneticField#getDeclination()}
	 */
	protected float declination;

	/**
	 * SensorManager for the compass mode of my location (phone's location).
	 * Registers the sensorListener.
	 */
	private SensorManager sensorManager;

	/**
	 * Handles the rotation in direction of the view (phone's orientation)
	 * referring to the compass mode after the specified amount of time elapses.
	 */
	private MySensorEventListener sensorListener;

	/**
	 * handles the stop of the compass mode if the user scrolls away on the map
	 * from my location (phone's location) so that the point of interest is no
	 * longer my location.
	 */
	private GestureDetector gestureDetector;

	private Toast toast;

	/**
	 * Constructor which sets the context and the fragmentManager of the context
	 * to show the map in a fragment of the mainActivity.
	 * 
	 * @param context
	 *            context of the map. Normally the {@link ims.main.MainActivity
	 *            MainActivity}
	 * @param fragmentManager
	 *            fragment manager of the context to show the map in a frame of
	 *            the context class
	 */
	public Map(Context context, FragmentManager fragmentManager) {
		this.context = context;
		this.fragmentManager = fragmentManager;
		gpsChildFinder = (GpsChildFinder) context.getApplicationContext();
	}

	/**
	 * Called in the noCreate() method in the context class (here the
	 * {@link ims.main.MainActivity MainActivity} to initialize the GoogleMap
	 * with the aid of the fragmentManager. <br>
	 * <br>
	 * Sets the GoogleMap type to satellite, initializes the
	 * {@link #gestureDetector} with the supplied listener and calls the methods
	 * to setup the main components of the GoogleMap:
	 * {@link #initMyLocationComponents()},
	 * {@link #initDeviceLocationComponents()},
	 * {@link #initDirectionComponents()} <br>
	 * Is everything set the map-screen is setup ({@link #startScreen()} method)
	 */
	public void initMap() {

		if (googleMap == null) {

			initMap = true;

			OnTouchMapFragment customMapFragment = ((OnTouchMapFragment) fragmentManager
					.findFragmentById(R.id.map));
			googleMap = customMapFragment.getMap();
			googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

			gestureDetector = new GestureDetector(context,
					new MapGestureListener());
			customMapFragment.setOnTouchListener(new MapOnTouchListener());

			initMyLocationComponents();
			initDeviceLocationComponents();
			initDirectionComponents();

		}
	}

	/**
	 * Is called when the map gets initialized. <br>
	 * Sets up the main components for screening my location (phone's location)
	 * and for handling the information and location.<br>
	 * The following methods are called:
	 * <ul>
	 * 1. {@link #setupMyLocationButton()} <br>
	 * 2. {@link #setupMyLocationIcon()} <br>
	 * 3. {@link #setupMyLocationClient()}<br>
	 * 4. {@link #setupSensorEventListener()} <br>
	 * </ul>
	 */
	private void initMyLocationComponents() {
		setupMyLocationButton();
		setupMyLocationIcon();
		setupMyLocationClient();
		setupSensorEventListener();
	}

	/**
	 * Is called when the map gets initialized. <br>
	 * Sets up the main components for screening the device location and for
	 * handling the information and location.<br>
	 * The following methods are called:
	 * <ul>
	 * 1. {@link #setupDeviceLocationButton()} <br>
	 * 2. {@link #setupDeviceLocationIcon()} <br>
	 * 3. {@link #setupDeviceLocationManager()} <br>
	 * 4. {@link #getDeviceLocation()} <br>
	 * </ul>
	 */
	private void initDeviceLocationComponents() {
		setupDeviceLocationButton();
		setupDeviceLocationIcon();
		setupDeviceLocationManager();
		getDeviceLocation();
	}

	/**
	 * Is called when the map gets initialized. <br>
	 * Sets up the TextView {@link #distance} and instantiates
	 * {@link ims.map.DirectionThread} object for handling the direction arrow
	 * and displayed distance.
	 */
	private void initDirectionComponents() {
		distance = (TextView) gpsChildFinder.getMainActivity().findViewById(
				R.id.distance);
		new DirectionThread(this, context);
	}

	/**
	 * Called then the map and its components get initialized. <br>
	 * Sets up the {@link #myLocationButton} for my location (phone's location). <br>
	 * Finds the view, registers the callback for click-events by setting an
	 * OnClickListener ({@link MyLocationButtonListener}).
	 */
	private void setupMyLocationButton() {
		myLocationButton = (MyLocationButton) gpsChildFinder.getMainActivity()
				.findViewById(R.id.my_location_button);

		myLocationButton.setOnClickListener(new MyLocationButtonListener(this,
				context));
	}

	/**
	 * Called then the map and its components get initialized. <br>
	 * Sets up the {@link #deviceLocationButton} for the location of the device. <br>
	 * Finds the view, registers the callback for click-events by setting an
	 * OnClickListener ({@link DeviceLocationButtonListener}).
	 */
	private void setupDeviceLocationButton() {
		deviceLocationButton = (DeviceLocationButton) gpsChildFinder
				.getMainActivity().findViewById(R.id.device_location_button);

		deviceLocationButton
				.setOnClickListener(new DeviceLocationButtonListener(this,
						context));
	}

	/**
	 * Wrapper function. Calls the {@link GoogleMap#setMapType(int)} method and
	 * forwards the map type to set the GoogleMap-type.
	 * 
	 * @param type
	 *            GoogleMap-type. Can either be
	 *            {@link GoogleMap#MAP_TYPE_SATELLITE},
	 *            {@link GoogleMap#MAP_TYPE_TERRAIN} or
	 *            {@link GoogleMap#MAP_TYPE_NORMAL}
	 */
	public void setMapType(int type) {
		googleMap.setMapType(type);
	}

	/**
	 * Called from the {@link #onConnected(Bundle)} method then the map and its
	 * components get initialized. <br>
	 * It's called there, because {@link #myLocationClient} should be connected.
	 * Prepares the starting screen/starting position. Starting position is my
	 * location (phone's location) on the map with the {@link #myLocationButton}
	 * enabled and so the compass mode activated.
	 */
	private void startScreen() {
		googleMap.moveCamera(CameraUpdateFactory.zoomTo(19));
		showMyLocation();
		initMap = false;
	}

	/**
	 * Called then the map and its components get initialized. <br>
	 * Sets the icon for the marker of my location (location of the phone) and
	 * its characteristics.
	 */
	private void setupMyLocationIcon() {
		myLocationMarkerOptions.icon(BitmapDescriptorFactory
				.fromResource(R.drawable.blue_pin_small));
		myLocationMarkerOptions.flat(true);
		myLocationMarkerOptions.anchor(0.5f, 0.5f);
	}

	/**
	 * Called then the map and its components get initialized. <br>
	 ** Icon for the marker of the location of the GPS-BT-device and its
	 * characteristics.
	 */
	private void setupDeviceLocationIcon() {
		deviceLocationMarkerOptions.icon(BitmapDescriptorFactory
				.fromResource(R.drawable.green_pin_small));
		deviceLocationMarkerOptions.flat(true);
		deviceLocationMarkerOptions.anchor(0.5f, 0.5f);
	}

	/**
	 * Called when the map and its components get initialized. <br>
	 * Sets up the {@link #myLocationClient} and tries to connect the client. <br>
	 * {@link MyLocationListener} is also set up which handles the changes of
	 * the location and the compass mode.
	 */
	private void setupMyLocationClient() {
		myLocationListener = new MyLocationListener(this);

		myLocationClient = new LocationClient(context, this, this);
		connectMyLocationClient();
	}

	/**
	 * Called from {@link ims.map.MyLocationListener} when a update of my
	 * location (phones location) is requested from the
	 * {@link #myLocationClient}.
	 * 
	 */
	public void requestMyLocationUpdate() {
		myLocationClient.getLastLocation();
	}

	/**
	 * Method first checks, if {@link #myLocationClient} is connected and if it
	 * got a location. Then calls my/phone's current location and asks for the
	 * last known GPS-coordinates. If {@link #showDirection} returns true
	 * {@link #showDirection()} is called, to update the direction arrow and the
	 * distance within; otherwise a marker for the location is directly added
	 * with {@link addMyLocationMarker(MarkerOptions)}.
	 */
	void getMyLocation() {
		if (myLocationClient != null && myLocationClient.isConnected()
				&& myLocationClient.getLastLocation() != null) {
			if (!myLocationButton.isEnabled()) {
				enableMyLocationButton();
			}
			if (myLocationLatLng == null) {
				Location myLocation = myLocationClient.getLastLocation();
				myLocationLatLng = new LatLng(myLocation.getLatitude(),
						myLocation.getLongitude());
			}

			myLocationMarkerOptions.position(myLocationLatLng);
			/**
			 * If the direction arrow and distance should be shown on the map
			 * (direction checkbox is checked in settings menu and a device is
			 * connected) its forwarded to {@link showDirection()} method.
			 */
			if (showDirection) {
				showDirection();
			} else {
				addMyLocationMarker(myLocationMarkerOptions);
			}
		} else {
			diableMyLocationButton(R.string.my_location_waiting_toast);
		}
	}

	/**
	 * Called then the map and its components get initialized. <br>
	 * Sets up the {@link #deviceLocationHandler} and register for location
	 * updates after a specific amount of time elapses with the
	 * {@link DeviceLocationListener} which handles the changes of the location.
	 */
	private void setupDeviceLocationManager() {
		deviceLocationHandler = new Handler(Looper.getMainLooper());
		updateDeviceLocationRunnable = new DeviceLocationListener(this, context);
		updateDeviceLocationRunnable.run();

	}

	/**
	 * Called then the map and its components get initialized. <br>
	 * Sets up the {@link MySensorEventListener} and registers it on
	 * {@link #sensorManager} for updating the compass mode and the rotation in
	 * direction of the view (phone's orientation).
	 */
	private void setupSensorEventListener() {
		sensorListener = new MySensorEventListener(this);

		sensorManager = (SensorManager) context
				.getSystemService(Context.SENSOR_SERVICE);
		if (sensorManager.getSensorList(Sensor.TYPE_ROTATION_VECTOR).size() != 0) {
			Sensor sensor = sensorManager.getSensorList(
					Sensor.TYPE_ROTATION_VECTOR).get(0);
			sensorManager.registerListener(sensorListener, sensor,
					SensorManager.SENSOR_DELAY_UI);
		}
	}

	/**
	 * If {@link #myLocationButton} is selected or unselected the button icon
	 * and status gets changed with
	 * {@link MyLocationButton#setSelected(boolean)} and
	 * {@link MyLocationButton#setButtonStatus(ButtonStatus)}. <br>
	 * Also the {@link #deviceLocationButton} methods are called for deselecting
	 * and button icon changing that both selected aren't possible.
	 * 
	 * @param selected
	 *            <b>true</b> if the myLocationButton is pressed/selected by the
	 *            user, <b>false</b> otherwise
	 */
	public void selectMyLocationButton(Boolean selected) {
		myLocationButton.setSelected(selected);
		if (selected == true) {
			myLocationButton.setButtonStatus(ButtonStatus.PRESSED);
			deviceLocationButton.setSelected(false);
			deviceLocationButton.setButtonStatus(ButtonStatus.UNPRESSED);
		}
		if (selected == false) {
			myLocationButton.setButtonStatus(ButtonStatus.UNPRESSED);
		}
	}

	/**
	 * If {@link #deviceLocationButton} is selected or unselected the button
	 * icon and status gets changed with
	 * {@link DeviceLocationButton#setSelected(boolean)} and
	 * {@link DeviceLocationButton#setButtonStatus(ButtonStatus)}. <br>
	 * Also the {@link #myLocationButton} methods are called for deselecting and
	 * button icon changing that both selected aren't possible.
	 * 
	 * @param selected
	 *            <b>true</b> if the deviceLocationButton is pressed/selected by
	 *            the user, <b>false</b> otherwise
	 */
	public void selectDeviceLocationButton(Boolean selected) {
		deviceLocationButton.setSelected(selected);
		if (selected == true) {
			deviceLocationButton.setButtonStatus(ButtonStatus.PRESSED);
			myLocationButton.setSelected(false);
			myLocationButton.setButtonStatus(ButtonStatus.UNPRESSED);
		}
		if (selected == false) {
			deviceLocationButton.setButtonStatus(ButtonStatus.UNPRESSED);
		}
	}

	/**
	 * Method places my/phone's current location as point of interest in the
	 * center of the view by moving the map's camera. Is also called if the
	 * {@link #myLocationButton} is pressed. <br>
	 * Calling the {@link #getMyLocation()} method if no location can be
	 * demanded.
	 */
	public void showMyLocation() {
		if (myLocationLatLng == null) {
			getMyLocation();
		}
		if (myLocationLatLng != null) {
			googleMap.moveCamera(CameraUpdateFactory
					.newLatLng(myLocationLatLng));
		}
	}

	/**
	 * Method places device's current location as point of interest in the
	 * center of the view by moving the map's camera. Is also called if the
	 * {@link #deviceLocationButton} is pressed. <br>
	 * Method returns when no device is connected or there is no GPS input from
	 * it. <br>
	 * Calling the {@link #getDeviceLocation()} method if no location can be
	 * demanded.
	 */
	public void showDeviceLocation(View view) {

		if (!BluetoothConnectThread.DEVICE_CONNECTED
				|| !BluetoothConnectThread.GETTING_GPS_INPUT) {
			return;
		}

		if (deviceLocLatLng == null) {
			getDeviceLocation();
		}

		if (deviceLocLatLng != null) {
			googleMap
					.moveCamera(CameraUpdateFactory.newLatLng(deviceLocLatLng));
		}
	}

	/**
	 * Method sends a one time request to the {@link #deviceLocationHandler}
	 * with the {@link #updateDeviceLocationRunnable} to get the device
	 * location's GPS-coordinates and updates the marker of the device's
	 * location on the map.
	 */
	private void getDeviceLocation() {

		deviceLocationHandler.postDelayed(updateDeviceLocationRunnable, 0);
	}

	/**
	 * Called when the direction should be shown on the GoogleMap if the my
	 * location (location of the phone) or device location is updated. <br>
	 * {@link #updateDistance(boolean)} and {@link #updateDirectionArrow()} is
	 * called from here.
	 */
	void showDirection() {
		updateDistance(false);
		updateDirectionArrow();
	}

	/**
	 * Called from {@link DirectionThread} class if the direction arrow should
	 * be shown on the marker of my location (blue pin). The icon of the
	 * myLocationMarker is updated (and set later on), {@link #showDirection()}
	 * is called and {@link #showDirection} is set true.
	 */
	void addDirection() {
		Log.d(getClass().getName(), "addDirection..");
		myLocationMarkerOptions.icon(BitmapDescriptorFactory
				.fromResource(R.drawable.blue_pin_with_direction_arrow));

		gpsChildFinder.getMainActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				showDirection();
			}
		});

		showDirection = true;
	}

	/**
	 * Called from {@link DirectionThread} class if the direction arrow should
	 * be removed from the marker of my location (blue pin). The icon of the
	 * myLocationMarker is updated, the distance textView is removed by {@link
	 * updateDistance(boolean)} and the {@link #showDirection} is set false.
	 */
	void removeDirection() {
		Log.d(getClass().getName(), "removeDirection..");
		myLocationMarkerOptions.icon(BitmapDescriptorFactory
				.fromResource(R.drawable.blue_pin_small));

		gpsChildFinder.getMainActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				updateDistance(true);
				addMyLocationMarker(myLocationMarkerOptions);
			}
		});

		showDirection = false;

	}

	/**
	 * Adds or rather updates the icon of the marker of my location (phone's
	 * location)
	 * 
	 * @param myLocationMarkerOptions
	 *            the options to add the marker of my location (phone's
	 *            location) with
	 */
	private synchronized void addMyLocationMarker(
			final MarkerOptions myLocationMarkerOptions) {
		if (myLocationMarkerOptions.getPosition() == null) {
			getMyLocation();
		}

		if (myLocationMarkerOptions.getPosition() != null) {

			if (myLocationMarker != null) {
				myLocationMarker.remove();
			}
			myLocationMarker = googleMap.addMarker(myLocationMarkerOptions);
		}
	}

	/**
	 * Adds or rather updates the icon of the marker of the device's location
	 * 
	 * @param deviceLocationMarkerOptions
	 *            the options to add the marker of the device's location
	 */
	public void addDeviceLocationMarker(
			MarkerOptions deviceLocationMarkerOptions) {
		if (deviceLocationMarkerOptions.getPosition() == null) {
			getDeviceLocation();
		}

		if (deviceLocationMarkerOptions.getPosition() != null) {
			if (deviceLocationMarker != null) {
				deviceLocationMarker.remove();
			}
			deviceLocationMarker = googleMap
					.addMarker(deviceLocationMarkerOptions);
		}

	}

	/**
	 * Removes the marker of the device's location when the location can't be
	 * obtained.
	 */
	public void removeDeviceLocationMarker() {
		if (deviceLocationMarker != null) {
			deviceLocationMarker.remove();
		}

	}

	/**
	 * Removes the marker of my location (phone's location) when the location
	 * can't be obtained.
	 */
	public void removeMyLocationMarker() {
		if (myLocationMarker != null)
			myLocationMarker.remove();

	}

	/**
	 * Is called when the distance should be updated. That happens when whether
	 * my location (phone's location) or the location of the GPS-device changes. <br>
	 * {@link Location#distanceBetween(double, double, double, double, float[])}
	 * is called to calculate the new distance in meters between the points.
	 * 
	 * @param remove
	 *            <b> true </b> if distance textView should be removed, <b>
	 *            false </b> if distance should be updated
	 */
	public synchronized void updateDistance(final boolean remove) {
		if (remove) {
			distance.setText("");
		} else {
			try {
				if (myLocationLatLng != null && deviceLocLatLng != null) {
					Location.distanceBetween(myLocationLatLng.latitude,
							myLocationLatLng.longitude,
							deviceLocLatLng.latitude,
							deviceLocLatLng.longitude, distanceInMeters);
					distance.setText(String.format("%.1f %s",
							distanceInMeters[0], " m"));
				}
			} catch (NullPointerException e) {
				Log.d("map.updateDistance()", "NullPointerException");
			}
		}
	}

	/**
	 * Called when the direction arrow should be updated to align in direction
	 * of the remote device. <br>
	 * <br>
	 * First, it stores the location of the device and myLocation locally, to
	 * avoid "half"-updates of a location. <br>
	 * <br>
	 * <b>Calculation of the rotation degrees:</b> <br>
	 * The angle is calculated with the help of the
	 * {@link Math#atan2(double, double)} method and the latitudes and
	 * longitudes of the two points/locations. <br>
	 * Than the rotation - how much the marker icon should be rotated to point
	 * to the device location - is calculated according to the cardinal
	 * direction of the device from the location of my phone (north-west,
	 * north-east, south-west or south-east of my location). Because the marker
	 * icon (the arrow) is standardly pointed to the top of the display, the
	 * calculated degrees have to be added or subtracted from an certain amount
	 * of degrees to get the correct degrees to rotate. <br>
	 * <br>
	 * Finally the {@link #myLocationMarkerOptions} are updated and
	 * {@link #addMyLocationMarker(MarkerOptions)} is called.
	 */
	public synchronized void updateDirectionArrow() {
		try {
			LatLng device = this.deviceLocLatLng;
			LatLng myLocation = this.myLocationLatLng;

			double angleInDegrees = Math.toDegrees(Math.atan2(
					Math.abs(device.latitude - myLocation.latitude),
					Math.abs(device.longitude - myLocation.longitude)));
			float rotation = 0.0f;

			// North of myLocation
			if (device.latitude >= myLocation.latitude) {
				// North-west of myLocation
				if (device.longitude >= myLocation.longitude) {
					rotation = (float) (90 - angleInDegrees);
				}
				// North-east of myLocation
				else {
					rotation = (float) (270 + angleInDegrees);
				}
			}
			// South of myLocation
			else {
				// South-west of myLocation
				if (device.longitude >= myLocation.longitude) {
					rotation = (float) (90 + angleInDegrees);
				}
				// South-east of myLocation
				else {
					rotation = (float) (270 - angleInDegrees);
				}
			}
			myLocationMarkerOptions.rotation(rotation);
			if (myLocationButton.isEnabled()) {
				addMyLocationMarker(myLocationMarkerOptions);
			}

		} catch (NullPointerException e) {
			Log.d("map.updateDirectionArrow()", "NullPointerException");
		}

	}

	/**
	 * Called from {@link ims.map.DeviceButtonListener} or
	 * {@link ims.map.MyLocationListener} when the new bounds of the screen
	 * should be calculated so that both - device and my location - are shown on
	 * the map.<br>
	 * The Method redirects to
	 * {@link BoundsCalculator#calculateNewBounds(GoogleMap, LatLng)} method.
	 * 
	 * @param listener
	 *            the listener where the method was called from
	 */
	public void calculateNewBounds(OnClickListener listener) {
		if (myLocationLatLng != null && deviceLocLatLng != null) {
			if (listener.getClass().equals(DeviceLocationButtonListener.class)) {
				BoundsCalculator
						.calculateNewBounds(googleMap, myLocationLatLng);
			} else if (listener.getClass().equals(
					MyLocationButtonListener.class)) {
				BoundsCalculator.calculateNewBounds(googleMap, deviceLocLatLng);
			}
		}
	}

	/**
	 * Called from {@link ims.map.MySensorEventListener} and moves the camera
	 * into a new position after the view direction has changed.
	 * 
	 * @param bearing
	 *            direction of the compass
	 */
	public void updateCamera(float bearing) {
		CameraPosition oldPos = googleMap.getCameraPosition();

		CameraPosition pos = CameraPosition.builder(oldPos).bearing(bearing)
				.build();
		googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(pos));
	}

	/**
	 * Shows a toast on the screen.
	 * 
	 * @param message
	 *            Current relevant message to show on the screen. The resource
	 *            id of the string resource to use. Can be formatted text.
	 * @param duration
	 *            How long to display the message. Either
	 *            {@link android.widget.Toast#LENGTH_SHORT Toast.LENGTH_SHORT}
	 *            or {@link android.widget.Toast.LENGTH_LONG Toast#LENGTH_LONG}
	 */
	public void showToast(int message, int duration) {
		toast = Toast.makeText(context, message, duration);
		toast.show();

	}

	/**
	 * Is called from {@link ims.main.MainActivity} when the Activity gets
	 * destroyed. <br>
	 * Removes the callbacks from the {@link #deviceLocationHandler}.
	 * Unregisters the {@link #sensorListener} from all sensors.
	 * 
	 * @throws NullPointerException
	 *             when the {@link #deviceLocationHandler} or
	 *             {@link #updateDeviceLocationRunnable} isn't initialized at
	 *             the moment.
	 */
	public void onDestroy() throws NullPointerException {
		deviceLocationHandler.removeCallbacks(updateDeviceLocationRunnable);
		sensorManager.unregisterListener(sensorListener);
		disconnectMyLocationClient();
	}

	/**
	 * 
	 * @return current deviceHandler
	 */
	public Handler getDeviceLocationHandler() {
		return deviceLocationHandler;
	}

	/**
	 * 
	 * @param deviceHandler
	 *            to set
	 */
	public void setDeviceHandler(Handler deviceHandler) {
		this.deviceLocationHandler = deviceHandler;
	}

	/**
	 * 
	 * @return current myLocationButton
	 */
	public MyLocationButton getMyLocationButton() {
		return myLocationButton;
	}

	/**
	 * 
	 * @param myLocationButton
	 *            to set
	 */
	public void setMyLocationButton(MyLocationButton myLocationButton) {
		this.myLocationButton = myLocationButton;
	}

	/**
	 * 
	 * @return current deviceLocationButton
	 */
	public DeviceLocationButton getDeviceLocationButton() {
		return deviceLocationButton;
	}

	/**
	 * 
	 * @param deviceLocationButton
	 *            to set
	 */
	public void setDeviceLocationButton(
			DeviceLocationButton deviceLocationButton) {
		this.deviceLocationButton = deviceLocationButton;
	}

	/**
	 * This class handles the fling-events.
	 * 
	 * @author Daniela Blum
	 * 
	 */
	private class MapGestureListener extends
			GestureDetector.SimpleOnGestureListener {

		/**
		 * If a fling event happens, the button which is selected gets
		 * unselected and the compass mode is stopped (the the point of interest
		 * location is no longer tracked and the center of the map's screen).
		 */
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			if (myLocationButton.isSelected()) {
				selectMyLocationButton(false);
				return true;
			} else if (deviceLocationButton.isSelected()) {
				selectDeviceLocationButton(false);
				return true;
			}
			return false;
		}
	}

	/**
	 * 
	 * This class handles the touch-events.
	 * 
	 * @author Daniela Blum
	 * 
	 */
	private class MapOnTouchListener implements OnTouchListener {

		/**
		 * Forwards the touch event to the {@link ims.map.Map#gestureDetector}.
		 */
		@Override
		public void onTouch(View v, MotionEvent motionEvent) {
			if (gestureDetector != null)
				gestureDetector.onTouchEvent(motionEvent);

		}

	}

	/**
	 * Called by Location Services when the request to connect the client
	 * finishes successfully. At this point, you can request the current
	 * location or start periodic updates.
	 */
	@Override
	public void onConnected(Bundle arg0) {
		myLocationClient.requestLocationUpdates(
				myLocationListener.getMyLocationRequest(), myLocationListener);
		if (initMap) {
			startScreen();
		}

		Log.i(getClass().getName(), "Connected to GooglePlayServicesClient");
	}

	/**
	 * Called by Location Services if the attempt to Location Services fails.<br>
	 * <br>
	 * Google Play services can resolve some errors it detects. If the error has
	 * a resolution, a activity starts that tries to resolve the error, an error
	 * dialog is shown otherwise.
	 */
	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		Log.i(getClass().getName(),
				"GooglePlayServicesClient: connection failed");
		if (connectionResult.hasResolution()) {
			try {
				connectionResult.startResolutionForResult(
						gpsChildFinder.getMainActivity(),
						CONNECTION_FAILURE_RESOLUTION_REQUEST);
			} catch (IntentSender.SendIntentException e) {
				diableMyLocationButton(R.string.location_client_OFF);
			}
		} else {
			diableMyLocationButton(R.string.location_client_OFF);

			GooglePlayServicesUtil.getErrorDialog(
					connectionResult.getErrorCode(),
					gpsChildFinder.getMainActivity(),
					CONNECTION_FAILURE_RESOLUTION_REQUEST).show();
		}
	}

	/**
	 * Called by Location Services if the connection to the location client
	 * drops because of an error.
	 */
	@Override
	public void onDisconnected() {
		myLocationButton.setButtonStatus(ButtonStatus.DISABLED);
		removeMyLocationMarker();
		diableMyLocationButton(R.string.location_client_OFF);
		Log.i(getClass().getName(),
				"GooglePlayServicesClient disconnected. Please re-connect.");

	}

	/**
	 * Connects the location client.
	 */
	public void connectMyLocationClient() {
		if (myLocationClient != null && !myLocationClient.isConnected()) {
			myLocationClient.connect();
		}
	}

	/**
	 * Disconnects the location client and removes all the location updates.
	 */
	public void disconnectMyLocationClient() {
		if (myLocationClient != null && myLocationClient.isConnected()) {
			myLocationClient.removeLocationUpdates(myLocationListener);
			myLocationClient.disconnect();
		}
	}

	/**
	 * Enables {@link ims.map.Map#myLocationButton} on the map to the state
	 * before it got disabled. This can either be
	 * {@link ims.map.buttons.StatusImageButton.ButtonStatus#PRESSED
	 * ButtonStatus.PRESSED} or
	 * {@link ims.map.buttons.StatusImageButton.ButtonStatus#UNPRESSED
	 * ButtonStatus.UNPRESSED}
	 */
	private void enableMyLocationButton() {
		myLocationButton.setEnabled(true);
		if (myLocationButton.isSelected()) {
			myLocationButton.setButtonStatus(ButtonStatus.PRESSED);
			showMyLocation();
		} else {
			myLocationButton.setButtonStatus(ButtonStatus.UNPRESSED);
			getMyLocation();
		}
	}

	/**
	 * Disabling the {@link #myLocationButton}, removing the marker from the map
	 * and showing a toast with the information for the user (why the button
	 * gets disabled).
	 * 
	 * @param toastMessage
	 */
	private void diableMyLocationButton(int toastMessage) {

		showToast(toastMessage, Toast.LENGTH_SHORT);

		myLocationButton.setButtonStatus(ButtonStatus.DISABLED);
		removeMyLocationMarker();

	}

	/**
	 * Checks which location settings are set on the phone which are needed to
	 * show my location (phone's location) on the map. <br>
	 * <br>
	 * For older versions than android KITKAT it uses a depreciated method.
	 * 
	 * @return <b>KITKAT and newer: true</b> if the location access is turned on
	 *         and the location mode is set to
	 *         {@link Settings.Secure#LOCATION_MODE_HIGH_ACCURACY} or
	 *         {@link Settings.Secure#LOCATION_MODE_BATTERY_SAVING},
	 *         <b>false</b> otherwise (location access off (
	 *         {@link Settings.Secure#LOCATION_MODE_OFF}) or device only) <br>
	 *         <b>older versions: true</b> if
	 *         {@link Settings.Secure#LOCATION_PROVIDERS_ALLOWED} contains at
	 *         least one provider, <b> false </b> if its empty.
	 */
	@TargetApi(Build.VERSION_CODES.KITKAT)
	@SuppressWarnings("deprecation")
	public boolean checkLocationSettings() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			try {
				locationMode = Settings.Secure.getInt(
						context.getContentResolver(),
						Settings.Secure.LOCATION_MODE);

			} catch (SettingNotFoundException e) {
				e.printStackTrace();
			}

			if (locationMode != Settings.Secure.LOCATION_MODE_OFF
					&& (locationMode == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY || locationMode == Settings.Secure.LOCATION_MODE_BATTERY_SAVING)) {
				return true;
			}

		} else {
			locationProviders = Settings.Secure.getString(
					context.getContentResolver(),
					Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
			if (!TextUtils.isEmpty(locationProviders)) {
				return true;
			}
		}

		diableMyLocationButton(R.string.location_access_OFF);
		return false;

	}

}
