package ims.map;

import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;
import ims.devicemanager.BluetoothConnectThread;
import ims.settings.SettingsActivity;

/**
 * Class checks continuously whether the distance (arrow and display of distance
 * on the map) should be shown by reference to the distance checkbox (settings
 * menu) and the status of the device (is the device connected?, is gps input
 * received?).<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class DirectionThread extends Thread {

	private Context context;
	private Map map;
	private static boolean showDirection;

	/**
	 * Constructor. Initializes the direction arrow and distance textView and
	 * starts the thread/run method.
	 * 
	 * @param map
	 *            Map class in which the main components of the map are located.
	 * @param context
	 *            map's and directionThread's context
	 */
	public DirectionThread(Map map, Context context) {
		this.context = context;
		this.map = map;
		initDirection();
		Thread t = new Thread(this);
		t.start();
	}

	/**
	 * Initializes the direction components (arrow and display of distance on
	 * the map) by calling the {@link ims.map.Map#addDirection()} or
	 * {@link ims.map.Map#removeDirection()} method depending on
	 * {@link #checkDirection()} when the DirectionThread is initialized.
	 */
	private void initDirection() {
		showDirection = checkDirection();
		if (showDirection) {
			map.addDirection();
		} else {
			map.removeDirection();
		}

	}

	/**
	 * Checks continuously whether the status of the {@link #showDirection}
	 * boolean have changed and calls the {@link ims.map.Map#addDirection()}
	 * (user wants direction components shown and the criteria are fulfilled) or
	 * {@link ims.map.Map#removeDirection()} method (user don't want direction
	 * to be shown or the criteria arn't fulfilled).<br>
	 * Direction components are the direction arrow and display of distance on
	 * the map (distance textView).
	 */
	public void run() {

		while (true) {
			if (checkDirection() != showDirection) {
				showDirection = !showDirection;
				if (showDirection) {
					map.addDirection();
				} else {
					map.removeDirection();
				}
			}

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
		}
	}

	/**
	 * Method checks if the direction arrow should be shown depending on the
	 * direction-checkbox status from settings (
	 * {@link ims.settings.SettingsActivity#DIRECTION_CHECKED DIRECTION_CHECKED}
	 * ) and the status of the device (
	 * {@link ims.devicemanager.BluetoothConnectThread#DEVICE_CONNECTED
	 * DEVICE_CONNECTED},
	 * {@link ims.devicemanager.BluetoothConnectThread#GETTING_GPS_INPUT
	 * GETTING_GPS_INPUT}).<br>
	 * <br>
	 * If {@link ims.settings.SettingsActivity#DIRECTION_CHECKED
	 * DIRECTION_CHECKED} is null, the shared preferences are called to ask for
	 * the value.
	 * 
	 * @return true if direction should/can be shown; <br>
	 *         false if direction shouldn't/can't be shown
	 */
	private boolean checkDirection() {
		try {
			if (!BluetoothConnectThread.DEVICE_CONNECTED) {
				return false;
			}
			if (SettingsActivity.DIRECTION_CHECKED
					&& BluetoothConnectThread.DEVICE_CONNECTED
					&& BluetoothConnectThread.GETTING_GPS_INPUT) {
				return true;
			}
			return false;
		} catch (NullPointerException e) {
			if (SettingsActivity.DIRECTION_CHECKED == null) {
				return PreferenceManager.getDefaultSharedPreferences(context)
						.getBoolean("pref_direction", true);
			}
			Log.d(getClass().getName(), "checkDirection() Nullpointer");
			return false;
		}
	}
}
