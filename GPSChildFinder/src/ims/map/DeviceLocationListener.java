package ims.map;

import ims.devicemanager.BluetoothConnectThread;
import ims.devicemanager.DeviceHandler;
import ims.main.GpsChildFinder;
import ims.map.buttons.StatusImageButton.ButtonStatus;
import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

/**
 * Runnable which handles location changes of the device's location.<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class DeviceLocationListener implements Runnable {

	private GpsChildFinder gpsChildFinder;

	/**
	 * Map class in which the main components of the map and the
	 * {@link ims.map.Map#deviceLocationHandler} is located, which handles this
	 * listener
	 */
	private Map map;

	/**
	 * Constructor.
	 * 
	 * @param map
	 *            Map class in which the main components of the map and the
	 *            {@link ims.map.Map#deviceHandler} are located.
	 * @param context
	 *            map's and locationListener's context
	 */
	public DeviceLocationListener(Map map, Context context) {
		this.gpsChildFinder = (GpsChildFinder) context.getApplicationContext();
		this.map = map;
	}

	/**
	 * Gets called from {@link ims.map.Map} class and is running after the
	 * specified amount of time elapses which is set by its handler. Executes
	 * the active part of the class. <br>
	 * <br>
	 * First it gets checked whether a device is connected to the phone and GPS
	 * input is received from it or not. <br>
	 * If a device is connected and GPS input is received, the
	 * {@link ims.map.Map#deviceLocationButton} is set up and the GPS
	 * coordinates are requested from the
	 * {@link ims.devicemanager.BluetoothDataManager}. The marker options for
	 * the device's location are set up, so that the marker can be shown on the
	 * map's screen ({@link ims.map.Map#showDeviceLocation(android.view.View)}). <br>
	 * If the direction ({@link ims.map.Map#showDirection}) should be shown, the
	 * {@link ims.map.Map#showDirection()} method is called. <br>
	 * <br>
	 * If no device is connected or no GPS input is received, the location
	 * marker is removed from the map's screen and the
	 * {@link ims.map.Map#deviceLocationButton} is set disabled. A toast with
	 * informations for the user is shown as well.
	 */
	@Override
	public void run() {
		if (BluetoothConnectThread.DEVICE_CONNECTED) {
			DeviceHandler.disconnectedToastShown(false);
			if (BluetoothConnectThread.GETTING_GPS_INPUT) {
				if (!map.getDeviceLocationButton().isEnabled()) {
					map.getDeviceLocationButton().setEnabled(true);
					map.getDeviceLocationButton().setButtonStatus(
							ButtonStatus.UNPRESSED);
				}
				map.deviceLocLatLng = new LatLng(DeviceHandler
						.getBtDataManager().getLatitude(), DeviceHandler
						.getBtDataManager().getLongitude());
				map.deviceLocationMarkerOptions.position(map.deviceLocLatLng);
				map.addDeviceLocationMarker(map.deviceLocationMarkerOptions);

				if (map.getDeviceLocationButton().isSelected()) {
					map.showDeviceLocation(null);
				}

				if (map.showDirection) {
					map.showDirection();
				}
			}
		} else {
			map.removeDeviceLocationMarker();
			if (map.getDeviceLocationButton().isEnabled()) {
				map.getDeviceLocationButton().setEnabled(false);
				map.getDeviceLocationButton().setButtonStatus(
						ButtonStatus.DISABLED);
			}
			if (!DeviceHandler.disconnectedToastShown()) {
				gpsChildFinder.getMainActivity().showDisconnectedToast();
				DeviceHandler.disconnectedToastShown(true);
			}
		}
		map.getDeviceLocationHandler().postDelayed(this, 500);
	}
}
