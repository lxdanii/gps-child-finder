package ims.map;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

/**
 * Wrapper layout for {@link ims.map.OnTouchMapFragment} to add an
 * OnTouchListener to the layout.<br>
 * <br>
 * Copyright &copy 2014 by Daniela Blum <br>
 * <br>
 * This file is part of GPSChildFinder.<br>
 * 
 * GPSChildFinder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a
 * href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 * 
 * @author Daniela Blum
 * @version 1.1
 * 
 */
public class MapWrapperLayout extends FrameLayout {

	/**
	 * Interface for the OnTouchListener
	 * 
	 * @author Daniela Blum
	 * 
	 */
	public interface OnTouchListener {
		/**
		 * Handling touch events
		 * 
		 * @param v
		 *            current view
		 * @param motionEvent
		 *            happening event
		 */
		public void onTouch(View v, MotionEvent motionEvent);
	}

	private OnTouchListener onTouchListener;

	/**
	 * Constructor
	 * 
	 * @param context
	 *            current context
	 */
	public MapWrapperLayout(Context context) {
		super(context);
	}

	/**
	 * Wraps the {@link OnTouchListener#onTouch(View, MotionEvent)} method.
	 */
	@Override
	public boolean dispatchTouchEvent(MotionEvent motionEvent) {
		if (onTouchListener != null) {
			onTouchListener.onTouch(getRootView(), motionEvent);
		}
		return super.dispatchTouchEvent(motionEvent);
	}

	/**
	 * Sets the {@link OnTouchListener}
	 */
	public void setOnTouchListener(OnTouchListener onTouchListener) {
		this.onTouchListener = onTouchListener;
	}
}
